/**
 * WsPatientHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package sigemsreeduc.sigegatepat.holders;

import sigemsreeduc.sigegatepat.WsPatient;

public final class WsPatientHolder implements javax.xml.rpc.holders.Holder {
    public WsPatient value;

    public WsPatientHolder() {
    }

    public WsPatient getValue() {
        return value;
    }

    public void setValue(WsPatient value) {
        this.value = value;
    }

    public WsPatientHolder(WsPatient value) {
        this.value = value;
    }

}
