/**
 * IntegerListeHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package sigemsreeduc.sigegatepat.holders;

import sigemsreeduc.sigegatepat.IntegerListe;

public final class IntegerListeHolder implements javax.xml.rpc.holders.Holder {
    public IntegerListe value;

    public IntegerListeHolder() {
    }

    public IntegerListeHolder(IntegerListe value) {
        this.value = value;
    }

}
