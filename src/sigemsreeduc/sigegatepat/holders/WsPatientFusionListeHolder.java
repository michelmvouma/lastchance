/**
 * WsPatientFusionListeHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package sigemsreeduc.sigegatepat.holders;

import sigemsreeduc.sigegatepat.WsPatientFusionListe;

public final class WsPatientFusionListeHolder implements javax.xml.rpc.holders.Holder {
    public WsPatientFusionListe value;

    public WsPatientFusionListeHolder() {
    }

    public WsPatientFusionListeHolder(WsPatientFusionListe value) {
        this.value = value;
    }

}
