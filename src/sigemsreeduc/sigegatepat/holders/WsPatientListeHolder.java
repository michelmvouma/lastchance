/**
 * WsPatientListeHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package sigemsreeduc.sigegatepat.holders;

import sigemsreeduc.sigegatepat.WsPatientListe;

public final class WsPatientListeHolder implements javax.xml.rpc.holders.Holder {
    public WsPatientListe value;

    public WsPatientListeHolder() {
    }

    public WsPatientListeHolder(WsPatientListe value) {
        this.value = value;
    }

}
