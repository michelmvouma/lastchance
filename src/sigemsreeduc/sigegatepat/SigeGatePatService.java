/**
 * SigeGatePatService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package sigemsreeduc.sigegatepat;

public interface SigeGatePatService extends javax.xml.rpc.Service {
    public java.lang.String getSigeGatePatAddress();

    public SigeGatePat_PortType getSigeGatePat() throws javax.xml.rpc.ServiceException;

    public SigeGatePat_PortType getSigeGatePat(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
