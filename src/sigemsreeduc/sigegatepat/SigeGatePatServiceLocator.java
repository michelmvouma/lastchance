/**
 * SigeGatePatServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package sigemsreeduc.sigegatepat;

public class SigeGatePatServiceLocator extends org.apache.axis.client.Service implements SigeGatePatService {

    public SigeGatePatServiceLocator() {
    }


    public SigeGatePatServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public SigeGatePatServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for SigeGatePat
    private java.lang.String SigeGatePat_address = "http://10.227.101.6:8080/sigems_stb/services/SigeGatePat";

    public java.lang.String getSigeGatePatAddress() {
        return SigeGatePat_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String SigeGatePatWSDDServiceName = "SigeGatePat";

    public java.lang.String getSigeGatePatWSDDServiceName() {
        return SigeGatePatWSDDServiceName;
    }

    public void setSigeGatePatWSDDServiceName(java.lang.String name) {
        SigeGatePatWSDDServiceName = name;
    }

    public SigeGatePat_PortType getSigeGatePat() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(SigeGatePat_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getSigeGatePat(endpoint);
    }

    public SigeGatePat_PortType getSigeGatePat(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            SigeGatePatSoapBindingStub _stub = new SigeGatePatSoapBindingStub(portAddress, this);
            _stub.setPortName(getSigeGatePatWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setSigeGatePatEndpointAddress(java.lang.String address) {
        SigeGatePat_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (SigeGatePat_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                SigeGatePatSoapBindingStub _stub = new SigeGatePatSoapBindingStub(new java.net.URL(SigeGatePat_address), this);
                _stub.setPortName(getSigeGatePatWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("SigeGatePat".equals(inputPortName)) {
            return getSigeGatePat();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://10.227.101.6:8080/sigems_stb/services/SigeGatePat", "SigeGatePatService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://10.227.101.6:8080/sigems_stb/services/SigeGatePat", "SigeGatePat"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("SigeGatePat".equals(portName)) {
            setSigeGatePatEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
