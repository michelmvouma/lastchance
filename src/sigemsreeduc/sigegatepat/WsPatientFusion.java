/**
 * WsPatientFusion.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package sigemsreeduc.sigegatepat;

public class WsPatientFusion  implements java.io.Serializable {
    private java.lang.Integer etat;

    private java.lang.Integer ippPrimaire;

    private java.lang.Integer ippSecondaire;

    private java.lang.String pourquoi;

    private java.lang.String quand;

    public WsPatientFusion() {
    }

    public WsPatientFusion(
           java.lang.Integer etat,
           java.lang.Integer ippPrimaire,
           java.lang.Integer ippSecondaire,
           java.lang.String pourquoi,
           java.lang.String quand) {
           this.etat = etat;
           this.ippPrimaire = ippPrimaire;
           this.ippSecondaire = ippSecondaire;
           this.pourquoi = pourquoi;
           this.quand = quand;
    }


    /**
     * Gets the etat value for this WsPatientFusion.
     * 
     * @return etat
     */
    public java.lang.Integer getEtat() {
        return etat;
    }


    /**
     * Sets the etat value for this WsPatientFusion.
     * 
     * @param etat
     */
    public void setEtat(java.lang.Integer etat) {
        this.etat = etat;
    }


    /**
     * Gets the ippPrimaire value for this WsPatientFusion.
     * 
     * @return ippPrimaire
     */
    public java.lang.Integer getIppPrimaire() {
        return ippPrimaire;
    }


    /**
     * Sets the ippPrimaire value for this WsPatientFusion.
     * 
     * @param ippPrimaire
     */
    public void setIppPrimaire(java.lang.Integer ippPrimaire) {
        this.ippPrimaire = ippPrimaire;
    }


    /**
     * Gets the ippSecondaire value for this WsPatientFusion.
     * 
     * @return ippSecondaire
     */
    public java.lang.Integer getIppSecondaire() {
        return ippSecondaire;
    }


    /**
     * Sets the ippSecondaire value for this WsPatientFusion.
     * 
     * @param ippSecondaire
     */
    public void setIppSecondaire(java.lang.Integer ippSecondaire) {
        this.ippSecondaire = ippSecondaire;
    }


    /**
     * Gets the pourquoi value for this WsPatientFusion.
     * 
     * @return pourquoi
     */
    public java.lang.String getPourquoi() {
        return pourquoi;
    }


    /**
     * Sets the pourquoi value for this WsPatientFusion.
     * 
     * @param pourquoi
     */
    public void setPourquoi(java.lang.String pourquoi) {
        this.pourquoi = pourquoi;
    }


    /**
     * Gets the quand value for this WsPatientFusion.
     * 
     * @return quand
     */
    public java.lang.String getQuand() {
        return quand;
    }


    /**
     * Sets the quand value for this WsPatientFusion.
     * 
     * @param quand
     */
    public void setQuand(java.lang.String quand) {
        this.quand = quand;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof WsPatientFusion)) return false;
        WsPatientFusion other = (WsPatientFusion) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.etat==null && other.getEtat()==null) || 
             (this.etat!=null &&
              this.etat.equals(other.getEtat()))) &&
            ((this.ippPrimaire==null && other.getIppPrimaire()==null) || 
             (this.ippPrimaire!=null &&
              this.ippPrimaire.equals(other.getIppPrimaire()))) &&
            ((this.ippSecondaire==null && other.getIppSecondaire()==null) || 
             (this.ippSecondaire!=null &&
              this.ippSecondaire.equals(other.getIppSecondaire()))) &&
            ((this.pourquoi==null && other.getPourquoi()==null) || 
             (this.pourquoi!=null &&
              this.pourquoi.equals(other.getPourquoi()))) &&
            ((this.quand==null && other.getQuand()==null) || 
             (this.quand!=null &&
              this.quand.equals(other.getQuand())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getEtat() != null) {
            _hashCode += getEtat().hashCode();
        }
        if (getIppPrimaire() != null) {
            _hashCode += getIppPrimaire().hashCode();
        }
        if (getIppSecondaire() != null) {
            _hashCode += getIppSecondaire().hashCode();
        }
        if (getPourquoi() != null) {
            _hashCode += getPourquoi().hashCode();
        }
        if (getQuand() != null) {
            _hashCode += getQuand().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WsPatientFusion.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:SigeGatePat", "WsPatientFusion"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("etat");
        elemField.setXmlName(new javax.xml.namespace.QName("", "etat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ippPrimaire");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ippPrimaire"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ippSecondaire");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ippSecondaire"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pourquoi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pourquoi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quand");
        elemField.setXmlName(new javax.xml.namespace.QName("", "quand"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
