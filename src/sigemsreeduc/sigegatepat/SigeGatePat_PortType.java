/**
 * SigeGatePat_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package sigemsreeduc.sigegatepat;

import sigemsreeduc.sigegatepat.holders.*;

public interface SigeGatePat_PortType extends java.rmi.Remote {
    public java.lang.String selectPatient(java.lang.String idBase, java.lang.Integer ipp, WsPatientHolder patient) throws java.rmi.RemoteException;
    public java.lang.String searchPatient(java.lang.String idBase, java.lang.String nomNaissance, java.lang.String nomUsuel, java.lang.String prenom, java.lang.String dateNaissance, WsPatientIdentificationListeHolder listePatients) throws java.rmi.RemoteException;
    public java.lang.String searchPatientPagine(java.lang.String idBase, java.lang.Integer page, java.lang.Integer nbLigne, java.lang.String nomNaissance, java.lang.String nomUsuel, java.lang.String prenom, java.lang.String dateNaissance, WsPatientListeHolder listePatients) throws java.rmi.RemoteException;
    public java.lang.String searchPatientCliniquePagine(java.lang.String idBase, java.lang.Integer page, java.lang.Integer nbLigne, java.lang.String nomNaissance, java.lang.String nomUsuel, java.lang.String prenom, java.lang.String dateNaissance, WsPatientListeHolder listePatients) throws java.rmi.RemoteException;
    public java.lang.String createPatient(java.lang.String idBase, WsPatient patient, javax.xml.rpc.holders.IntegerWrapperHolder ipp) throws java.rmi.RemoteException;
    public java.lang.String createPatientHomonyme(java.lang.String idBase, WsPatient patient, javax.xml.rpc.holders.IntegerWrapperHolder ipp) throws java.rmi.RemoteException;
    public java.lang.String updatePatient(java.lang.String idBase, WsPatient patient) throws java.rmi.RemoteException;
    public java.lang.String selectPatientModifie(java.lang.String idBase, java.lang.Integer page, java.lang.Integer nbLigne, java.lang.String dateInf, java.lang.String heureInf, java.lang.String dateSup, java.lang.String heureSup, WsPatientListeHolder listePatients) throws java.rmi.RemoteException;
    public java.lang.String fusionnerPatient(java.lang.String idBase, java.lang.Integer ippMaitre, java.lang.Integer ippSecondaire) throws java.rmi.RemoteException;
    public java.lang.String defusionnerPatient(java.lang.String idBase, java.lang.Integer ippMaitre, java.lang.Integer ippSecondaire) throws java.rmi.RemoteException;
    public java.lang.String selectLinkedIpp(java.lang.String idBase, java.lang.Integer ipp, IntegerListeHolder listeIpp) throws java.rmi.RemoteException;
    public java.lang.String selectPatientFusionModifie(java.lang.String idBase, java.lang.String dateInf, java.lang.String dateSup, WsPatientFusionListeHolder listePatientFusion) throws java.rmi.RemoteException;
    public java.lang.String isPatientValid(java.lang.String idBase, java.lang.Integer ipp, javax.xml.rpc.holders.BooleanHolder result) throws java.rmi.RemoteException;
}
