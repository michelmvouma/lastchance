/**
 * WsPatient.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package sigemsreeduc.sigegatepat;

public class WsPatient  extends WsPatientIdentification implements java.io.Serializable {
    private java.lang.String adresse1;

    private java.lang.String adresse2;

    private java.lang.String cleSecu;

    private java.lang.String codePostal;

    private java.lang.String numSecu;

    private java.lang.String sexe;

    private java.lang.String telephone;

    private java.lang.String ville;

    private java.lang.String villeNaissance;

    public WsPatient() {
    }

    @Override
    public String toString() {
        return "WsPatient{" +
                "adresse1='" + adresse1 + '\'' +
                ", adresse2='" + adresse2 + '\'' +
                ", cleSecu='" + cleSecu + '\'' +
                ", codePostal='" + codePostal + '\'' +
                ", numSecu='" + numSecu + '\'' +
                ", sexe='" + sexe + '\'' +
                ", telephone='" + telephone + '\'' +
                ", ville='" + ville + '\'' +
                ", villeNaissance='" + villeNaissance + '\'' +
                ", __equalsCalc=" + __equalsCalc +
                ", __hashCodeCalc=" + __hashCodeCalc +

                ", Nom naissance ='" + super.getNomNaissance() + '\'' +
                ", Nom Usuelle ='" + super.getNomUsuel() + '\'' +
                ", Prenom Usuelle='" + super.getPrenom() + '\'' +
                '}';
    }

    public WsPatient(
           java.lang.String dateNaissance,
           java.lang.String ippExtern,
           java.lang.Integer ippSigems,
           java.lang.String nomNaissance,
           java.lang.String nomUsuel,
           java.lang.String prenom,
           java.lang.String adresse1,
           java.lang.String adresse2,
           java.lang.String cleSecu,
           java.lang.String codePostal,
           java.lang.String numSecu,
           java.lang.String sexe,
           java.lang.String telephone,
           java.lang.String ville,
           java.lang.String villeNaissance) {
        super(
            dateNaissance,
            ippExtern,
            ippSigems,
            nomNaissance,
            nomUsuel,
            prenom);
        this.adresse1 = adresse1;
        this.adresse2 = adresse2;
        this.cleSecu = cleSecu;
        this.codePostal = codePostal;
        this.numSecu = numSecu;
        this.sexe = sexe;
        this.telephone = telephone;
        this.ville = ville;
        this.villeNaissance = villeNaissance;
    }


    /**
     * Gets the adresse1 value for this WsPatient.
     * 
     * @return adresse1
     */
    public java.lang.String getAdresse1() {
        return adresse1;
    }


    /**
     * Sets the adresse1 value for this WsPatient.
     * 
     * @param adresse1
     */
    public void setAdresse1(java.lang.String adresse1) {
        this.adresse1 = adresse1;
    }


    /**
     * Gets the adresse2 value for this WsPatient.
     * 
     * @return adresse2
     */
    public java.lang.String getAdresse2() {
        return adresse2;
    }


    /**
     * Sets the adresse2 value for this WsPatient.
     * 
     * @param adresse2
     */
    public void setAdresse2(java.lang.String adresse2) {
        this.adresse2 = adresse2;
    }


    /**
     * Gets the cleSecu value for this WsPatient.
     * 
     * @return cleSecu
     */
    public java.lang.String getCleSecu() {
        return cleSecu;
    }


    /**
     * Sets the cleSecu value for this WsPatient.
     * 
     * @param cleSecu
     */
    public void setCleSecu(java.lang.String cleSecu) {
        this.cleSecu = cleSecu;
    }


    /**
     * Gets the codePostal value for this WsPatient.
     * 
     * @return codePostal
     */
    public java.lang.String getCodePostal() {
        return codePostal;
    }


    /**
     * Sets the codePostal value for this WsPatient.
     * 
     * @param codePostal
     */
    public void setCodePostal(java.lang.String codePostal) {
        this.codePostal = codePostal;
    }


    /**
     * Gets the numSecu value for this WsPatient.
     * 
     * @return numSecu
     */
    public java.lang.String getNumSecu() {
        return numSecu;
    }


    /**
     * Sets the numSecu value for this WsPatient.
     * 
     * @param numSecu
     */
    public void setNumSecu(java.lang.String numSecu) {
        this.numSecu = numSecu;
    }


    /**
     * Gets the sexe value for this WsPatient.
     * 
     * @return sexe
     */
    public java.lang.String getSexe() {
        return sexe;
    }


    /**
     * Sets the sexe value for this WsPatient.
     * 
     * @param sexe
     */
    public void setSexe(java.lang.String sexe) {
        this.sexe = sexe;
    }


    /**
     * Gets the telephone value for this WsPatient.
     * 
     * @return telephone
     */
    public java.lang.String getTelephone() {
        return telephone;
    }


    /**
     * Sets the telephone value for this WsPatient.
     * 
     * @param telephone
     */
    public void setTelephone(java.lang.String telephone) {
        this.telephone = telephone;
    }


    /**
     * Gets the ville value for this WsPatient.
     * 
     * @return ville
     */
    public java.lang.String getVille() {
        return ville;
    }


    /**
     * Sets the ville value for this WsPatient.
     * 
     * @param ville
     */
    public void setVille(java.lang.String ville) {
        this.ville = ville;
    }


    /**
     * Gets the villeNaissance value for this WsPatient.
     * 
     * @return villeNaissance
     */
    public java.lang.String getVilleNaissance() {
        return villeNaissance;
    }


    /**
     * Sets the villeNaissance value for this WsPatient.
     * 
     * @param villeNaissance
     */
    public void setVilleNaissance(java.lang.String villeNaissance) {
        this.villeNaissance = villeNaissance;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof WsPatient)) return false;
        WsPatient other = (WsPatient) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.adresse1==null && other.getAdresse1()==null) || 
             (this.adresse1!=null &&
              this.adresse1.equals(other.getAdresse1()))) &&
            ((this.adresse2==null && other.getAdresse2()==null) || 
             (this.adresse2!=null &&
              this.adresse2.equals(other.getAdresse2()))) &&
            ((this.cleSecu==null && other.getCleSecu()==null) || 
             (this.cleSecu!=null &&
              this.cleSecu.equals(other.getCleSecu()))) &&
            ((this.codePostal==null && other.getCodePostal()==null) || 
             (this.codePostal!=null &&
              this.codePostal.equals(other.getCodePostal()))) &&
            ((this.numSecu==null && other.getNumSecu()==null) || 
             (this.numSecu!=null &&
              this.numSecu.equals(other.getNumSecu()))) &&
            ((this.sexe==null && other.getSexe()==null) || 
             (this.sexe!=null &&
              this.sexe.equals(other.getSexe()))) &&
            ((this.telephone==null && other.getTelephone()==null) || 
             (this.telephone!=null &&
              this.telephone.equals(other.getTelephone()))) &&
            ((this.ville==null && other.getVille()==null) || 
             (this.ville!=null &&
              this.ville.equals(other.getVille()))) &&
            ((this.villeNaissance==null && other.getVilleNaissance()==null) || 
             (this.villeNaissance!=null &&
              this.villeNaissance.equals(other.getVilleNaissance())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAdresse1() != null) {
            _hashCode += getAdresse1().hashCode();
        }
        if (getAdresse2() != null) {
            _hashCode += getAdresse2().hashCode();
        }
        if (getCleSecu() != null) {
            _hashCode += getCleSecu().hashCode();
        }
        if (getCodePostal() != null) {
            _hashCode += getCodePostal().hashCode();
        }
        if (getNumSecu() != null) {
            _hashCode += getNumSecu().hashCode();
        }
        if (getSexe() != null) {
            _hashCode += getSexe().hashCode();
        }
        if (getTelephone() != null) {
            _hashCode += getTelephone().hashCode();
        }
        if (getVille() != null) {
            _hashCode += getVille().hashCode();
        }
        if (getVilleNaissance() != null) {
            _hashCode += getVilleNaissance().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WsPatient.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:SigeGatePat", "WsPatient"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("adresse1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "adresse1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("adresse2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "adresse2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cleSecu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cleSecu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codePostal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codePostal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numSecu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numSecu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sexe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sexe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("telephone");
        elemField.setXmlName(new javax.xml.namespace.QName("", "telephone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ville");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ville"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("villeNaissance");
        elemField.setXmlName(new javax.xml.namespace.QName("", "villeNaissance"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
