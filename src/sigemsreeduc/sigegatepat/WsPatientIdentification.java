/**
 * WsPatientIdentification.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package sigemsreeduc.sigegatepat;

public class WsPatientIdentification  implements java.io.Serializable {
    private java.lang.String dateNaissance;

    private java.lang.String ippExtern;

    private java.lang.Integer ippSigems;

    private java.lang.String nomNaissance;

    private java.lang.String nomUsuel;

    private java.lang.String prenom;

    public WsPatientIdentification() {
    }

    public WsPatientIdentification(
           java.lang.String dateNaissance,
           java.lang.String ippExtern,
           java.lang.Integer ippSigems,
           java.lang.String nomNaissance,
           java.lang.String nomUsuel,
           java.lang.String prenom) {
           this.dateNaissance = dateNaissance;
           this.ippExtern = ippExtern;
           this.ippSigems = ippSigems;
           this.nomNaissance = nomNaissance;
           this.nomUsuel = nomUsuel;
           this.prenom = prenom;
    }


    /**
     * Gets the dateNaissance value for this WsPatientIdentification.
     * 
     * @return dateNaissance
     */
    public java.lang.String getDateNaissance() {
        return dateNaissance;
    }


    /**
     * Sets the dateNaissance value for this WsPatientIdentification.
     * 
     * @param dateNaissance
     */
    public void setDateNaissance(java.lang.String dateNaissance) {
        this.dateNaissance = dateNaissance;
    }


    /**
     * Gets the ippExtern value for this WsPatientIdentification.
     * 
     * @return ippExtern
     */
    public java.lang.String getIppExtern() {
        return ippExtern;
    }


    /**
     * Sets the ippExtern value for this WsPatientIdentification.
     * 
     * @param ippExtern
     */
    public void setIppExtern(java.lang.String ippExtern) {
        this.ippExtern = ippExtern;
    }


    /**
     * Gets the ippSigems value for this WsPatientIdentification.
     * 
     * @return ippSigems
     */
    public java.lang.Integer getIppSigems() {
        return ippSigems;
    }


    /**
     * Sets the ippSigems value for this WsPatientIdentification.
     * 
     * @param ippSigems
     */
    public void setIppSigems(java.lang.Integer ippSigems) {
        this.ippSigems = ippSigems;
    }


    /**
     * Gets the nomNaissance value for this WsPatientIdentification.
     * 
     * @return nomNaissance
     */
    public java.lang.String getNomNaissance() {
        return nomNaissance;
    }


    /**
     * Sets the nomNaissance value for this WsPatientIdentification.
     * 
     * @param nomNaissance
     */
    public void setNomNaissance(java.lang.String nomNaissance) {
        this.nomNaissance = nomNaissance;
    }


    /**
     * Gets the nomUsuel value for this WsPatientIdentification.
     * 
     * @return nomUsuel
     */
    public java.lang.String getNomUsuel() {
        return nomUsuel;
    }


    /**
     * Sets the nomUsuel value for this WsPatientIdentification.
     * 
     * @param nomUsuel
     */
    public void setNomUsuel(java.lang.String nomUsuel) {
        this.nomUsuel = nomUsuel;
    }


    /**
     * Gets the prenom value for this WsPatientIdentification.
     * 
     * @return prenom
     */
    public java.lang.String getPrenom() {
        return prenom;
    }


    /**
     * Sets the prenom value for this WsPatientIdentification.
     * 
     * @param prenom
     */
    public void setPrenom(java.lang.String prenom) {
        this.prenom = prenom;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof WsPatientIdentification)) return false;
        WsPatientIdentification other = (WsPatientIdentification) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.dateNaissance==null && other.getDateNaissance()==null) || 
             (this.dateNaissance!=null &&
              this.dateNaissance.equals(other.getDateNaissance()))) &&
            ((this.ippExtern==null && other.getIppExtern()==null) || 
             (this.ippExtern!=null &&
              this.ippExtern.equals(other.getIppExtern()))) &&
            ((this.ippSigems==null && other.getIppSigems()==null) || 
             (this.ippSigems!=null &&
              this.ippSigems.equals(other.getIppSigems()))) &&
            ((this.nomNaissance==null && other.getNomNaissance()==null) || 
             (this.nomNaissance!=null &&
              this.nomNaissance.equals(other.getNomNaissance()))) &&
            ((this.nomUsuel==null && other.getNomUsuel()==null) || 
             (this.nomUsuel!=null &&
              this.nomUsuel.equals(other.getNomUsuel()))) &&
            ((this.prenom==null && other.getPrenom()==null) || 
             (this.prenom!=null &&
              this.prenom.equals(other.getPrenom())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDateNaissance() != null) {
            _hashCode += getDateNaissance().hashCode();
        }
        if (getIppExtern() != null) {
            _hashCode += getIppExtern().hashCode();
        }
        if (getIppSigems() != null) {
            _hashCode += getIppSigems().hashCode();
        }
        if (getNomNaissance() != null) {
            _hashCode += getNomNaissance().hashCode();
        }
        if (getNomUsuel() != null) {
            _hashCode += getNomUsuel().hashCode();
        }
        if (getPrenom() != null) {
            _hashCode += getPrenom().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WsPatientIdentification.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:SigeGatePat", "WsPatientIdentification"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateNaissance");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dateNaissance"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ippExtern");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ippExtern"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ippSigems");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ippSigems"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomNaissance");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomNaissance"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomUsuel");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nomUsuel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prenom");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prenom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
