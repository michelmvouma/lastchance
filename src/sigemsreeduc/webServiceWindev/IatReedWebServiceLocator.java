/**
 * IatReedWebServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package sigemsreeduc.webServiceWindev;

public class IatReedWebServiceLocator extends org.apache.axis.client.Service implements sigemsreeduc.webServiceWindev.IatReedWebService {

    public IatReedWebServiceLocator() {
    }


    public IatReedWebServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public IatReedWebServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for iatReedWebServiceSOAPPort
    private java.lang.String iatReedWebServiceSOAPPort_address = "http://172.16.11.76/IATREEDWEBSERVICE_WEB/awws/iatReedWebService.awws";

    public java.lang.String getiatReedWebServiceSOAPPortAddress() {
        return iatReedWebServiceSOAPPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String iatReedWebServiceSOAPPortWSDDServiceName = "iatReedWebServiceSOAPPort";

    public java.lang.String getiatReedWebServiceSOAPPortWSDDServiceName() {
        return iatReedWebServiceSOAPPortWSDDServiceName;
    }

    public void setiatReedWebServiceSOAPPortWSDDServiceName(java.lang.String name) {
        iatReedWebServiceSOAPPortWSDDServiceName = name;
    }

    public sigemsreeduc.webServiceWindev.IatReedWebServiceSOAPPortType getiatReedWebServiceSOAPPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(iatReedWebServiceSOAPPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getiatReedWebServiceSOAPPort(endpoint);
    }

    public sigemsreeduc.webServiceWindev.IatReedWebServiceSOAPPortType getiatReedWebServiceSOAPPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            sigemsreeduc.webServiceWindev.IatReedWebServiceSOAPBindingStub _stub = new sigemsreeduc.webServiceWindev.IatReedWebServiceSOAPBindingStub(portAddress, this);
            _stub.setPortName(getiatReedWebServiceSOAPPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setiatReedWebServiceSOAPPortEndpointAddress(java.lang.String address) {
        iatReedWebServiceSOAPPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (sigemsreeduc.webServiceWindev.IatReedWebServiceSOAPPortType.class.isAssignableFrom(serviceEndpointInterface)) {
                sigemsreeduc.webServiceWindev.IatReedWebServiceSOAPBindingStub _stub = new sigemsreeduc.webServiceWindev.IatReedWebServiceSOAPBindingStub(new java.net.URL(iatReedWebServiceSOAPPort_address), this);
                _stub.setPortName(getiatReedWebServiceSOAPPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("iatReedWebServiceSOAPPort".equals(inputPortName)) {
            return getiatReedWebServiceSOAPPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("urn:iatReedWebService", "iatReedWebService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("urn:iatReedWebService", "iatReedWebServiceSOAPPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("iatReedWebServiceSOAPPort".equals(portName)) {
            setiatReedWebServiceSOAPPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
