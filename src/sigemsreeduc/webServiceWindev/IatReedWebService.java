/**
 * IatReedWebService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package sigemsreeduc.webServiceWindev;

public interface IatReedWebService extends javax.xml.rpc.Service {
    public java.lang.String getiatReedWebServiceSOAPPortAddress();

    public sigemsreeduc.webServiceWindev.IatReedWebServiceSOAPPortType getiatReedWebServiceSOAPPort() throws javax.xml.rpc.ServiceException;

    public sigemsreeduc.webServiceWindev.IatReedWebServiceSOAPPortType getiatReedWebServiceSOAPPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
