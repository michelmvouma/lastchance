/**
 * IatReedWebServiceSOAPPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package sigemsreeduc.webServiceWindev;

public interface IatReedWebServiceSOAPPortType extends java.rmi.Remote {
    public java.lang.String insertionDonneesDansLesTables(java.lang.String dataASauvegarder, int nNbreElement, java.lang.String sTypeEtab, java.lang.String sNomEtab, java.lang.String dateDebutS, java.lang.String dateFin) throws java.rmi.RemoteException;

    /**
     * R&eacute;sum&eacute; : &lt;indiquez ici ce que fait la proc&eacute;dure&gt;<BR>
     * Syntaxe :<BR>[ &lt;R&eacute;sultat&gt; = ] testTauxMontant ()<BR><BR>
     * Param&egrave;tres :<BR>	Aucun<BR> Valeur de retour :<BR> 	cha&#238;ne
     * : // 	Aucune<BR><BR> Exemple :<BR> Indiquez ici un exemple d'utilisation.<BR>
     */
    public java.lang.String testTauxMontant() throws java.rmi.RemoteException;

    /**
     * R&eacute;sum&eacute; : &lt;indiquez ici ce que fait la proc&eacute;dure&gt;<BR>
     * Syntaxe :<BR>[ &lt;R&eacute;sultat&gt; = ] testInsertionData ()<BR><BR>
     * Param&egrave;tres :<BR>	Aucun<BR> Valeur de retour :<BR> 	cha&#238;ne
     * : // 	Aucune<BR><BR> Exemple :<BR> Indiquez ici un exemple d'utilisation.<BR>
     */
    public java.lang.String testInsertionData() throws java.rmi.RemoteException;
}
