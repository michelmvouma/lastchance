/**
 * WsActeNgap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package sigemsreeduc.acte;

public class WsActeNgap  implements java.io.Serializable {
    private java.lang.String adeliPratExec;

    private java.lang.String adeliPratPrescr;

    private java.lang.String codePratExec;

    private java.lang.String codePratPrescr;

    private java.lang.String codeRejet;

    private java.lang.Float coefficient;

    private java.lang.Float denombremement;

    private java.lang.String executeDate;

    private java.lang.String executeHeure;

    private java.lang.String executionDimancheJourFerie;

    private java.lang.String executionNuit;

    private java.lang.Integer idActe;

    private java.lang.Integer ippSigems;

    private java.lang.String lettreCle;

    private java.lang.Float montantTotal;

    private java.lang.String numDossier;

    private java.lang.Integer numeroForfaitTechnique;

    private java.lang.Float quantite;

    private java.lang.String uniteMedicale;

    public WsActeNgap() {
    }

    public WsActeNgap(
           java.lang.String adeliPratExec,
           java.lang.String adeliPratPrescr,
           java.lang.String codePratExec,
           java.lang.String codePratPrescr,
           java.lang.String codeRejet,
           java.lang.Float coefficient,
           java.lang.Float denombremement,
           java.lang.String executeDate,
           java.lang.String executeHeure,
           java.lang.String executionDimancheJourFerie,
           java.lang.String executionNuit,
           java.lang.Integer idActe,
           java.lang.Integer ippSigems,
           java.lang.String lettreCle,
           java.lang.Float montantTotal,
           java.lang.String numDossier,
           java.lang.Integer numeroForfaitTechnique,
           java.lang.Float quantite,
           java.lang.String uniteMedicale) {
           this.adeliPratExec = adeliPratExec;
           this.adeliPratPrescr = adeliPratPrescr;
           this.codePratExec = codePratExec; //codePratExec
           this.codePratPrescr = codePratPrescr;
           this.codeRejet = codeRejet;
           this.coefficient = coefficient;
           this.denombremement = denombremement;
           this.executeDate = executeDate;
           this.executeHeure = executeHeure;
           this.executionDimancheJourFerie = executionDimancheJourFerie;
           this.executionNuit = executionNuit;
           this.idActe = idActe;
           this.ippSigems = ippSigems;
           this.lettreCle = lettreCle;
           this.montantTotal = montantTotal;
           this.numDossier = numDossier;
           this.numeroForfaitTechnique = numeroForfaitTechnique;
           this.quantite = quantite;
           this.uniteMedicale = uniteMedicale;
    }


    /**
     * Gets the adeliPratExec value for this WsActeNgap.
     * 
     * @return adeliPratExec
     */
    public java.lang.String getAdeliPratExec() {
        return adeliPratExec;
    }


    /**
     * Sets the adeliPratExec value for this WsActeNgap.
     * 
     * @param adeliPratExec
     */
    public void setAdeliPratExec(java.lang.String adeliPratExec) {
        this.adeliPratExec = adeliPratExec;
    }


    /**
     * Gets the adeliPratPrescr value for this WsActeNgap.
     * 
     * @return adeliPratPrescr
     */
    public java.lang.String getAdeliPratPrescr() {
        return adeliPratPrescr;
    }


    /**
     * Sets the adeliPratPrescr value for this WsActeNgap.
     * 
     * @param adeliPratPrescr
     */
    public void setAdeliPratPrescr(java.lang.String adeliPratPrescr) {
        this.adeliPratPrescr = adeliPratPrescr;
    }


    /**
     * Gets the codePratExec value for this WsActeNgap.
     * 
     * @return codePratExec
     */
    public java.lang.String getCodePratExec() {
        return codePratExec;
    }


    /**
     * Sets the codePratExec value for this WsActeNgap.
     * 
     * @param codePratExec
     */
    public void setCodePratExec(java.lang.String codePratExec) {
        this.codePratExec = codePratExec;
    }


    /**
     * Gets the codePratPrescr value for this WsActeNgap.
     * 
     * @return codePratPrescr
     */
    public java.lang.String getCodePratPrescr() {
        return codePratPrescr;
    }


    /**
     * Sets the codePratPrescr value for this WsActeNgap.
     * 
     * @param codePratPrescr
     */
    public void setCodePratPrescr(java.lang.String codePratPrescr) {
        this.codePratPrescr = codePratPrescr;
    }


    /**
     * Gets the codeRejet value for this WsActeNgap.
     * 
     * @return codeRejet
     */
    public java.lang.String getCodeRejet() {
        return codeRejet;
    }


    /**
     * Sets the codeRejet value for this WsActeNgap.
     * 
     * @param codeRejet
     */
    public void setCodeRejet(java.lang.String codeRejet) {
        this.codeRejet = codeRejet;
    }


    /**
     * Gets the coefficient value for this WsActeNgap.
     * 
     * @return coefficient
     */
    public java.lang.Float getCoefficient() {
        return coefficient;
    }


    /**
     * Sets the coefficient value for this WsActeNgap.
     * 
     * @param coefficient
     */
    public void setCoefficient(java.lang.Float coefficient) {
        this.coefficient = coefficient;
    }


    /**
     * Gets the denombremement value for this WsActeNgap.
     * 
     * @return denombremement
     */
    public java.lang.Float getDenombremement() {
        return denombremement;
    }


    /**
     * Sets the denombremement value for this WsActeNgap.
     * 
     * @param denombremement
     */
    public void setDenombremement(java.lang.Float denombremement) {
        this.denombremement = denombremement;
    }


    /**
     * Gets the executeDate value for this WsActeNgap.
     * 
     * @return executeDate
     */
    public java.lang.String getExecuteDate() {
        return executeDate;
    }


    /**
     * Sets the executeDate value for this WsActeNgap.
     * 
     * @param executeDate
     */
    public void setExecuteDate(java.lang.String executeDate) {
        this.executeDate = executeDate;
    }


    /**
     * Gets the executeHeure value for this WsActeNgap.
     * 
     * @return executeHeure
     */
    public java.lang.String getExecuteHeure() {
        return executeHeure;
    }


    /**
     * Sets the executeHeure value for this WsActeNgap.
     * 
     * @param executeHeure
     */
    public void setExecuteHeure(java.lang.String executeHeure) {
        this.executeHeure = executeHeure;
    }


    /**
     * Gets the executionDimancheJourFerie value for this WsActeNgap.
     * 
     * @return executionDimancheJourFerie
     */
    public java.lang.String getExecutionDimancheJourFerie() {
        return executionDimancheJourFerie;
    }


    /**
     * Sets the executionDimancheJourFerie value for this WsActeNgap.
     * 
     * @param executionDimancheJourFerie
     */
    public void setExecutionDimancheJourFerie(java.lang.String executionDimancheJourFerie) {
        this.executionDimancheJourFerie = executionDimancheJourFerie;
    }


    /**
     * Gets the executionNuit value for this WsActeNgap.
     * 
     * @return executionNuit
     */
    public java.lang.String getExecutionNuit() {
        return executionNuit;
    }


    /**
     * Sets the executionNuit value for this WsActeNgap.
     * 
     * @param executionNuit
     */
    public void setExecutionNuit(java.lang.String executionNuit) {
        this.executionNuit = executionNuit;
    }


    /**
     * Gets the idActe value for this WsActeNgap.
     * 
     * @return idActe
     */
    public java.lang.Integer getIdActe() {
        return idActe;
    }


    /**
     * Sets the idActe value for this WsActeNgap.
     * 
     * @param idActe
     */
    public void setIdActe(java.lang.Integer idActe) {
        this.idActe = idActe;
    }


    /**
     * Gets the ippSigems value for this WsActeNgap.
     * 
     * @return ippSigems
     */
    public java.lang.Integer getIppSigems() {
        return ippSigems;
    }


    /**
     * Sets the ippSigems value for this WsActeNgap.
     * 
     * @param ippSigems
     */
    public void setIppSigems(java.lang.Integer ippSigems) {
        this.ippSigems = ippSigems;
    }


    /**
     * Gets the lettreCle value for this WsActeNgap.
     * 
     * @return lettreCle
     */
    public java.lang.String getLettreCle() {
        return lettreCle;
    }


    /**
     * Sets the lettreCle value for this WsActeNgap.
     * 
     * @param lettreCle
     */
    public void setLettreCle(java.lang.String lettreCle) {
        this.lettreCle = lettreCle;
    }


    /**
     * Gets the montantTotal value for this WsActeNgap.
     * 
     * @return montantTotal
     */
    public java.lang.Float getMontantTotal() {
        return montantTotal;
    }


    /**
     * Sets the montantTotal value for this WsActeNgap.
     * 
     * @param montantTotal
     */
    public void setMontantTotal(java.lang.Float montantTotal) {
        this.montantTotal = montantTotal;
    }


    /**
     * Gets the numDossier value for this WsActeNgap.
     * 
     * @return numDossier
     */
    public java.lang.String getNumDossier() {
        return numDossier;
    }


    /**
     * Sets the numDossier value for this WsActeNgap.
     * 
     * @param numDossier
     */
    public void setNumDossier(java.lang.String numDossier) {
        this.numDossier = numDossier;
    }


    /**
     * Gets the numeroForfaitTechnique value for this WsActeNgap.
     * 
     * @return numeroForfaitTechnique
     */
    public java.lang.Integer getNumeroForfaitTechnique() {
        return numeroForfaitTechnique;
    }


    /**
     * Sets the numeroForfaitTechnique value for this WsActeNgap.
     * 
     * @param numeroForfaitTechnique
     */
    public void setNumeroForfaitTechnique(java.lang.Integer numeroForfaitTechnique) {
        this.numeroForfaitTechnique = numeroForfaitTechnique;
    }


    /**
     * Gets the quantite value for this WsActeNgap.
     * 
     * @return quantite
     */
    public java.lang.Float getQuantite() {
        return quantite;
    }


    /**
     * Sets the quantite value for this WsActeNgap.
     * 
     * @param quantite
     */
    public void setQuantite(java.lang.Float quantite) {
        this.quantite = quantite;
    }


    /**
     * Gets the uniteMedicale value for this WsActeNgap.
     * 
     * @return uniteMedicale
     */
    public java.lang.String getUniteMedicale() {
        return uniteMedicale;
    }


    /**
     * Sets the uniteMedicale value for this WsActeNgap.
     * 
     * @param uniteMedicale
     */
    public void setUniteMedicale(java.lang.String uniteMedicale) {
        this.uniteMedicale = uniteMedicale;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof WsActeNgap)) return false;
        WsActeNgap other = (WsActeNgap) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.adeliPratExec==null && other.getAdeliPratExec()==null) || 
             (this.adeliPratExec!=null &&
              this.adeliPratExec.equals(other.getAdeliPratExec()))) &&
            ((this.adeliPratPrescr==null && other.getAdeliPratPrescr()==null) || 
             (this.adeliPratPrescr!=null &&
              this.adeliPratPrescr.equals(other.getAdeliPratPrescr()))) &&
            ((this.codePratExec==null && other.getCodePratExec()==null) || 
             (this.codePratExec!=null &&
              this.codePratExec.equals(other.getCodePratExec()))) &&
            ((this.codePratPrescr==null && other.getCodePratPrescr()==null) || 
             (this.codePratPrescr!=null &&
              this.codePratPrescr.equals(other.getCodePratPrescr()))) &&
            ((this.codeRejet==null && other.getCodeRejet()==null) || 
             (this.codeRejet!=null &&
              this.codeRejet.equals(other.getCodeRejet()))) &&
            ((this.coefficient==null && other.getCoefficient()==null) || 
             (this.coefficient!=null &&
              this.coefficient.equals(other.getCoefficient()))) &&
            ((this.denombremement==null && other.getDenombremement()==null) || 
             (this.denombremement!=null &&
              this.denombremement.equals(other.getDenombremement()))) &&
            ((this.executeDate==null && other.getExecuteDate()==null) || 
             (this.executeDate!=null &&
              this.executeDate.equals(other.getExecuteDate()))) &&
            ((this.executeHeure==null && other.getExecuteHeure()==null) || 
             (this.executeHeure!=null &&
              this.executeHeure.equals(other.getExecuteHeure()))) &&
            ((this.executionDimancheJourFerie==null && other.getExecutionDimancheJourFerie()==null) || 
             (this.executionDimancheJourFerie!=null &&
              this.executionDimancheJourFerie.equals(other.getExecutionDimancheJourFerie()))) &&
            ((this.executionNuit==null && other.getExecutionNuit()==null) || 
             (this.executionNuit!=null &&
              this.executionNuit.equals(other.getExecutionNuit()))) &&
            ((this.idActe==null && other.getIdActe()==null) || 
             (this.idActe!=null &&
              this.idActe.equals(other.getIdActe()))) &&
            ((this.ippSigems==null && other.getIppSigems()==null) || 
             (this.ippSigems!=null &&
              this.ippSigems.equals(other.getIppSigems()))) &&
            ((this.lettreCle==null && other.getLettreCle()==null) || 
             (this.lettreCle!=null &&
              this.lettreCle.equals(other.getLettreCle()))) &&
            ((this.montantTotal==null && other.getMontantTotal()==null) || 
             (this.montantTotal!=null &&
              this.montantTotal.equals(other.getMontantTotal()))) &&
            ((this.numDossier==null && other.getNumDossier()==null) || 
             (this.numDossier!=null &&
              this.numDossier.equals(other.getNumDossier()))) &&
            ((this.numeroForfaitTechnique==null && other.getNumeroForfaitTechnique()==null) || 
             (this.numeroForfaitTechnique!=null &&
              this.numeroForfaitTechnique.equals(other.getNumeroForfaitTechnique()))) &&
            ((this.quantite==null && other.getQuantite()==null) || 
             (this.quantite!=null &&
              this.quantite.equals(other.getQuantite()))) &&
            ((this.uniteMedicale==null && other.getUniteMedicale()==null) || 
             (this.uniteMedicale!=null &&
              this.uniteMedicale.equals(other.getUniteMedicale())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAdeliPratExec() != null) {
            _hashCode += getAdeliPratExec().hashCode();
        }
        if (getAdeliPratPrescr() != null) {
            _hashCode += getAdeliPratPrescr().hashCode();
        }
        if (getCodePratExec() != null) {
            _hashCode += getCodePratExec().hashCode();
        }
        if (getCodePratPrescr() != null) {
            _hashCode += getCodePratPrescr().hashCode();
        }
        if (getCodeRejet() != null) {
            _hashCode += getCodeRejet().hashCode();
        }
        if (getCoefficient() != null) {
            _hashCode += getCoefficient().hashCode();
        }
        if (getDenombremement() != null) {
            _hashCode += getDenombremement().hashCode();
        }
        if (getExecuteDate() != null) {
            _hashCode += getExecuteDate().hashCode();
        }
        if (getExecuteHeure() != null) {
            _hashCode += getExecuteHeure().hashCode();
        }
        if (getExecutionDimancheJourFerie() != null) {
            _hashCode += getExecutionDimancheJourFerie().hashCode();
        }
        if (getExecutionNuit() != null) {
            _hashCode += getExecutionNuit().hashCode();
        }
        if (getIdActe() != null) {
            _hashCode += getIdActe().hashCode();
        }
        if (getIppSigems() != null) {
            _hashCode += getIppSigems().hashCode();
        }
        if (getLettreCle() != null) {
            _hashCode += getLettreCle().hashCode();
        }
        if (getMontantTotal() != null) {
            _hashCode += getMontantTotal().hashCode();
        }
        if (getNumDossier() != null) {
            _hashCode += getNumDossier().hashCode();
        }
        if (getNumeroForfaitTechnique() != null) {
            _hashCode += getNumeroForfaitTechnique().hashCode();
        }
        if (getQuantite() != null) {
            _hashCode += getQuantite().hashCode();
        }
        if (getUniteMedicale() != null) {
            _hashCode += getUniteMedicale().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WsActeNgap.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:SigeGateActeNgap", "WsActeNgap"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("adeliPratExec");
        elemField.setXmlName(new javax.xml.namespace.QName("", "adeliPratExec"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("adeliPratPrescr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "adeliPratPrescr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codePratExec");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codePratExec"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codePratPrescr");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codePratPrescr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codeRejet");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codeRejet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("coefficient");
        elemField.setXmlName(new javax.xml.namespace.QName("", "coefficient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "float"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("denombremement");
        elemField.setXmlName(new javax.xml.namespace.QName("", "denombremement"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "float"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("executeDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "executeDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("executeHeure");
        elemField.setXmlName(new javax.xml.namespace.QName("", "executeHeure"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("executionDimancheJourFerie");
        elemField.setXmlName(new javax.xml.namespace.QName("", "executionDimancheJourFerie"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("executionNuit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "executionNuit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idActe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "idActe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ippSigems");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ippSigems"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lettreCle");
        elemField.setXmlName(new javax.xml.namespace.QName("", "lettreCle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("montantTotal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "montantTotal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "float"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numDossier");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numDossier"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numeroForfaitTechnique");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numeroForfaitTechnique"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quantite");
        elemField.setXmlName(new javax.xml.namespace.QName("", "quantite"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "float"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uniteMedicale");
        elemField.setXmlName(new javax.xml.namespace.QName("", "uniteMedicale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    @Override
    public String toString() {
        return "WsActeNgap{" +
                "adeliPratExec='" + adeliPratExec + '\'' +
                ", adeliPratPrescr='" + adeliPratPrescr + '\'' +
                ", codePratExec='" + codePratExec + '\'' +
                ", codePratPrescr='" + codePratPrescr + '\'' +
                ", codeRejet='" + codeRejet + '\'' +
                ", coefficient=" + coefficient +
                ", denombremement=" + denombremement +
                ", executeDate='" + executeDate + '\'' +
                ", executeHeure='" + executeHeure + '\'' +
                ", executionDimancheJourFerie='" + executionDimancheJourFerie + '\'' +
                ", executionNuit='" + executionNuit + '\'' +
                ", idActe=" + idActe +
                ", ippSigems=" + ippSigems +
                ", lettreCle='" + lettreCle + '\'' +
                ", montantTotal=" + montantTotal +
                ", numDossier='" + numDossier + '\'' +
                ", numeroForfaitTechnique=" + numeroForfaitTechnique +
                ", quantite=" + quantite +
                ", uniteMedicale='" + uniteMedicale + '\'' +
                ", __equalsCalc=" + __equalsCalc +
                ", __hashCodeCalc=" + __hashCodeCalc +
                '}';
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
