/**
 * SigeGateActeNgapServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package sigemsreeduc.acte;

public class SigeGateActeNgapServiceLocator extends org.apache.axis.client.Service implements sigemsreeduc.acte.SigeGateActeNgapService {

    public SigeGateActeNgapServiceLocator() {
    }


    public SigeGateActeNgapServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public SigeGateActeNgapServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for SigeGateActeNgap
    private java.lang.String SigeGateActeNgap_address = "http://10.227.101.6:8080/sigems_stb/services/SigeGateActeNgap";

    public java.lang.String getSigeGateActeNgapAddress() {
        return SigeGateActeNgap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String SigeGateActeNgapWSDDServiceName = "SigeGateActeNgap";

    public java.lang.String getSigeGateActeNgapWSDDServiceName() {
        return SigeGateActeNgapWSDDServiceName;
    }

    public void setSigeGateActeNgapWSDDServiceName(java.lang.String name) {
        SigeGateActeNgapWSDDServiceName = name;
    }

    public sigemsreeduc.acte.SigeGateActeNgap_PortType getSigeGateActeNgap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(SigeGateActeNgap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getSigeGateActeNgap(endpoint);
    }

    public sigemsreeduc.acte.SigeGateActeNgap_PortType getSigeGateActeNgap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            sigemsreeduc.acte.SigeGateActeNgapSoapBindingStub _stub = new sigemsreeduc.acte.SigeGateActeNgapSoapBindingStub(portAddress, this);
            _stub.setPortName(getSigeGateActeNgapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setSigeGateActeNgapEndpointAddress(java.lang.String address) {
        SigeGateActeNgap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (sigemsreeduc.acte.SigeGateActeNgap_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                sigemsreeduc.acte.SigeGateActeNgapSoapBindingStub _stub = new sigemsreeduc.acte.SigeGateActeNgapSoapBindingStub(new java.net.URL(SigeGateActeNgap_address), this);
                _stub.setPortName(getSigeGateActeNgapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("SigeGateActeNgap".equals(inputPortName)) {
            return getSigeGateActeNgap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://10.227.101.6:8080/sigems_stb/services/SigeGateActeNgap", "SigeGateActeNgapService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://10.227.101.6:8080/sigems_stb/services/SigeGateActeNgap", "SigeGateActeNgap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("SigeGateActeNgap".equals(portName)) {
            setSigeGateActeNgapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
