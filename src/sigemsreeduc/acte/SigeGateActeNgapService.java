/**
 * SigeGateActeNgapService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package sigemsreeduc.acte;

public interface SigeGateActeNgapService extends javax.xml.rpc.Service {
    public java.lang.String getSigeGateActeNgapAddress();

    public sigemsreeduc.acte.SigeGateActeNgap_PortType getSigeGateActeNgap() throws javax.xml.rpc.ServiceException;

    public sigemsreeduc.acte.SigeGateActeNgap_PortType getSigeGateActeNgap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
