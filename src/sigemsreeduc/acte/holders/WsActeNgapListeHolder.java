/**
 * WsActeNgapListeHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package sigemsreeduc.acte.holders;

import sigemsreeduc.acte.WsActeNgapListe;

public final class WsActeNgapListeHolder implements javax.xml.rpc.holders.Holder {
    public sigemsreeduc.acte.WsActeNgapListe value;

    public WsActeNgapListeHolder() {
    }

    public WsActeNgapListe getValue() {
        return value;
    }

    public void setValue(WsActeNgapListe value) {
        this.value = value;
    }

    public WsActeNgapListeHolder(sigemsreeduc.acte.WsActeNgapListe value) {
        this.value = value;
    }

}
