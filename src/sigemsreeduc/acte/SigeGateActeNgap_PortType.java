/**
 * SigeGateActeNgap_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package sigemsreeduc.acte;

public interface SigeGateActeNgap_PortType extends java.rmi.Remote {
    public java.lang.String searchActe(java.lang.String idBase, java.lang.Integer page, java.lang.Integer nbLigne, sigemsreeduc.acte.WsActeNgap acte, java.lang.String dateInf, java.lang.String dateSup, sigemsreeduc.acte.holders.WsActeNgapListeHolder listeActes) throws java.rmi.RemoteException;
    public java.lang.String searchActeTemp(java.lang.String idBase, java.lang.Integer page, java.lang.Integer nbLigne, sigemsreeduc.acte.WsActeNgap acte, java.lang.String dateInf, java.lang.String dateSup, sigemsreeduc.acte.holders.WsActeNgapListeHolder listeActes) throws java.rmi.RemoteException;
    public java.lang.String createActeTemp(java.lang.String idBase, sigemsreeduc.acte.WsActeNgap acte) throws java.rmi.RemoteException;
    public java.lang.String createActesTempParDossier(java.lang.String idBase, sigemsreeduc.acte.WsActeNgapListe listeActes) throws java.rmi.RemoteException;
    public java.lang.String deleteActeTemp(java.lang.String idBase, java.lang.Integer idActe) throws java.rmi.RemoteException;
}
