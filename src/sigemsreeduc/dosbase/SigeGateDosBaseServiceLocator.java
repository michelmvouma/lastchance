/**
 * SigeGateDosBaseServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package sigemsreeduc.dosbase;

public class SigeGateDosBaseServiceLocator extends org.apache.axis.client.Service implements sigemsreeduc.dosbase.SigeGateDosBaseService {

    public SigeGateDosBaseServiceLocator() {
    }


    public SigeGateDosBaseServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public SigeGateDosBaseServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for SigeGateDosBase
    private java.lang.String SigeGateDosBase_address = "http://10.227.101.6:8080/sigems_stb/services/SigeGateDosBase";

    public java.lang.String getSigeGateDosBaseAddress() {
        return SigeGateDosBase_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String SigeGateDosBaseWSDDServiceName = "SigeGateDosBase";

    public java.lang.String getSigeGateDosBaseWSDDServiceName() {
        return SigeGateDosBaseWSDDServiceName;
    }

    public void setSigeGateDosBaseWSDDServiceName(java.lang.String name) {
        SigeGateDosBaseWSDDServiceName = name;
    }

    public sigemsreeduc.dosbase.SigeGateDosBase_PortType getSigeGateDosBase() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(SigeGateDosBase_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getSigeGateDosBase(endpoint);
    }

    public sigemsreeduc.dosbase.SigeGateDosBase_PortType getSigeGateDosBase(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            sigemsreeduc.dosbase.SigeGateDosBaseSoapBindingStub _stub = new sigemsreeduc.dosbase.SigeGateDosBaseSoapBindingStub(portAddress, this);
            _stub.setPortName(getSigeGateDosBaseWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setSigeGateDosBaseEndpointAddress(java.lang.String address) {
        SigeGateDosBase_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (sigemsreeduc.dosbase.SigeGateDosBase_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                sigemsreeduc.dosbase.SigeGateDosBaseSoapBindingStub _stub = new sigemsreeduc.dosbase.SigeGateDosBaseSoapBindingStub(new java.net.URL(SigeGateDosBase_address), this);
                _stub.setPortName(getSigeGateDosBaseWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("SigeGateDosBase".equals(inputPortName)) {
            return getSigeGateDosBase();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://10.227.101.6:8080/sigems_stb/services/SigeGateDosBase", "SigeGateDosBaseService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://10.227.101.6:8080/sigems_stb/services/SigeGateDosBase", "SigeGateDosBase"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("SigeGateDosBase".equals(portName)) {
            setSigeGateDosBaseEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
