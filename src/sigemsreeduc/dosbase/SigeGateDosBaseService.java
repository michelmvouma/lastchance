/**
 * SigeGateDosBaseService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package sigemsreeduc.dosbase;

public interface SigeGateDosBaseService extends javax.xml.rpc.Service {
    public java.lang.String getSigeGateDosBaseAddress();

    public sigemsreeduc.dosbase.SigeGateDosBase_PortType getSigeGateDosBase() throws javax.xml.rpc.ServiceException;

    public sigemsreeduc.dosbase.SigeGateDosBase_PortType getSigeGateDosBase(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
