/**
 * WsPersonne.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package sigemsreeduc.dosbase;

public class WsPersonne  implements java.io.Serializable {
    private java.lang.String adresse;

    private java.lang.String codePostal;

    private java.lang.String commune;

    private java.lang.String lien;

    private java.lang.String nom;

    private java.lang.String prenom;

    private java.lang.String telFixe;

    private java.lang.String telMobile;

    public WsPersonne() {
    }

    public WsPersonne(
           java.lang.String adresse,
           java.lang.String codePostal,
           java.lang.String commune,
           java.lang.String lien,
           java.lang.String nom,
           java.lang.String prenom,
           java.lang.String telFixe,
           java.lang.String telMobile) {
           this.adresse = adresse;
           this.codePostal = codePostal;
           this.commune = commune;
           this.lien = lien;
           this.nom = nom;
           this.prenom = prenom;
           this.telFixe = telFixe;
           this.telMobile = telMobile;
    }


    /**
     * Gets the adresse value for this WsPersonne.
     * 
     * @return adresse
     */
    public java.lang.String getAdresse() {
        return adresse;
    }


    /**
     * Sets the adresse value for this WsPersonne.
     * 
     * @param adresse
     */
    public void setAdresse(java.lang.String adresse) {
        this.adresse = adresse;
    }


    /**
     * Gets the codePostal value for this WsPersonne.
     * 
     * @return codePostal
     */
    public java.lang.String getCodePostal() {
        return codePostal;
    }


    /**
     * Sets the codePostal value for this WsPersonne.
     * 
     * @param codePostal
     */
    public void setCodePostal(java.lang.String codePostal) {
        this.codePostal = codePostal;
    }


    /**
     * Gets the commune value for this WsPersonne.
     * 
     * @return commune
     */
    public java.lang.String getCommune() {
        return commune;
    }


    /**
     * Sets the commune value for this WsPersonne.
     * 
     * @param commune
     */
    public void setCommune(java.lang.String commune) {
        this.commune = commune;
    }


    /**
     * Gets the lien value for this WsPersonne.
     * 
     * @return lien
     */
    public java.lang.String getLien() {
        return lien;
    }


    /**
     * Sets the lien value for this WsPersonne.
     * 
     * @param lien
     */
    public void setLien(java.lang.String lien) {
        this.lien = lien;
    }


    /**
     * Gets the nom value for this WsPersonne.
     * 
     * @return nom
     */
    public java.lang.String getNom() {
        return nom;
    }


    /**
     * Sets the nom value for this WsPersonne.
     * 
     * @param nom
     */
    public void setNom(java.lang.String nom) {
        this.nom = nom;
    }


    /**
     * Gets the prenom value for this WsPersonne.
     * 
     * @return prenom
     */
    public java.lang.String getPrenom() {
        return prenom;
    }


    /**
     * Sets the prenom value for this WsPersonne.
     * 
     * @param prenom
     */
    public void setPrenom(java.lang.String prenom) {
        this.prenom = prenom;
    }


    /**
     * Gets the telFixe value for this WsPersonne.
     * 
     * @return telFixe
     */
    public java.lang.String getTelFixe() {
        return telFixe;
    }


    /**
     * Sets the telFixe value for this WsPersonne.
     * 
     * @param telFixe
     */
    public void setTelFixe(java.lang.String telFixe) {
        this.telFixe = telFixe;
    }


    /**
     * Gets the telMobile value for this WsPersonne.
     * 
     * @return telMobile
     */
    public java.lang.String getTelMobile() {
        return telMobile;
    }


    /**
     * Sets the telMobile value for this WsPersonne.
     * 
     * @param telMobile
     */
    public void setTelMobile(java.lang.String telMobile) {
        this.telMobile = telMobile;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof WsPersonne)) return false;
        WsPersonne other = (WsPersonne) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.adresse==null && other.getAdresse()==null) || 
             (this.adresse!=null &&
              this.adresse.equals(other.getAdresse()))) &&
            ((this.codePostal==null && other.getCodePostal()==null) || 
             (this.codePostal!=null &&
              this.codePostal.equals(other.getCodePostal()))) &&
            ((this.commune==null && other.getCommune()==null) || 
             (this.commune!=null &&
              this.commune.equals(other.getCommune()))) &&
            ((this.lien==null && other.getLien()==null) || 
             (this.lien!=null &&
              this.lien.equals(other.getLien()))) &&
            ((this.nom==null && other.getNom()==null) || 
             (this.nom!=null &&
              this.nom.equals(other.getNom()))) &&
            ((this.prenom==null && other.getPrenom()==null) || 
             (this.prenom!=null &&
              this.prenom.equals(other.getPrenom()))) &&
            ((this.telFixe==null && other.getTelFixe()==null) || 
             (this.telFixe!=null &&
              this.telFixe.equals(other.getTelFixe()))) &&
            ((this.telMobile==null && other.getTelMobile()==null) || 
             (this.telMobile!=null &&
              this.telMobile.equals(other.getTelMobile())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAdresse() != null) {
            _hashCode += getAdresse().hashCode();
        }
        if (getCodePostal() != null) {
            _hashCode += getCodePostal().hashCode();
        }
        if (getCommune() != null) {
            _hashCode += getCommune().hashCode();
        }
        if (getLien() != null) {
            _hashCode += getLien().hashCode();
        }
        if (getNom() != null) {
            _hashCode += getNom().hashCode();
        }
        if (getPrenom() != null) {
            _hashCode += getPrenom().hashCode();
        }
        if (getTelFixe() != null) {
            _hashCode += getTelFixe().hashCode();
        }
        if (getTelMobile() != null) {
            _hashCode += getTelMobile().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WsPersonne.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:SigeGateDosBase", "WsPersonne"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("adresse");
        elemField.setXmlName(new javax.xml.namespace.QName("", "adresse"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codePostal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codePostal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commune");
        elemField.setXmlName(new javax.xml.namespace.QName("", "commune"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lien");
        elemField.setXmlName(new javax.xml.namespace.QName("", "lien"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nom");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prenom");
        elemField.setXmlName(new javax.xml.namespace.QName("", "prenom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("telFixe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "telFixe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("telMobile");
        elemField.setXmlName(new javax.xml.namespace.QName("", "telMobile"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

    @Override
    public String toString() {
        return "WsPersonne{" +
                "adresse='" + adresse + '\'' +
                ", codePostal='" + codePostal + '\'' +
                ", commune='" + commune + '\'' +
                ", lien='" + lien + '\'' +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", telFixe='" + telFixe + '\'' +
                ", telMobile='" + telMobile + '\'' +
                ", __equalsCalc=" + __equalsCalc +
                ", __hashCodeCalc=" + __hashCodeCalc +
                '}';
    }
}
