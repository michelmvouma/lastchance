/**
 * StringListeHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package sigemsreeduc.dosbase.holders;

public final class StringListeHolder implements javax.xml.rpc.holders.Holder {
    public sigemsreeduc.dosbase.StringListe value;

    public StringListeHolder() {
    }

    public StringListeHolder(sigemsreeduc.dosbase.StringListe value) {
        this.value = value;
    }

}
