/**
 * WsDossierBaseHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package sigemsreeduc.dosbase.holders;

import sigemsreeduc.dosbase.WsDossierBase;

public final class WsDossierBaseHolder implements javax.xml.rpc.holders.Holder {
    public sigemsreeduc.dosbase.WsDossierBase value;

    public WsDossierBaseHolder() {
    }

    public WsDossierBaseHolder(sigemsreeduc.dosbase.WsDossierBase value) {
        this.value = value;
    }

    public WsDossierBase getValue() {
        return value;
    }

    public void setValue(WsDossierBase value) {
        this.value = value;
    }
}
