/**
 * WsDossierBaseListeHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package sigemsreeduc.dosbase.holders;

import sigemsreeduc.dosbase.WsDossierBaseListe;

public final class WsDossierBaseListeHolder implements javax.xml.rpc.holders.Holder {
    public sigemsreeduc.dosbase.WsDossierBaseListe value;

    public WsDossierBaseListeHolder() {
    }

    public WsDossierBaseListe getValue() {
        return value;
    }

    public void setValue(WsDossierBaseListe value) {
        this.value = value;
    }

    public WsDossierBaseListeHolder(sigemsreeduc.dosbase.WsDossierBaseListe value) {
        this.value = value;
    }

}
