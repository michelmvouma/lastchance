/**
 * WsDossierBase.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package sigemsreeduc.dosbase;

import java.util.Arrays;

public class WsDossierBase  implements java.io.Serializable {
    private java.lang.String codeMedecinResponsable;

    private java.lang.String codeMedecinTraitant;

    private java.lang.String dateEntree;

    private java.lang.String dateSortiePrevue;

    private java.lang.String dateSortieReelle;

    private java.lang.String heureEntree;

    private java.lang.String heureSortiePrevue;

    private java.lang.String heureSortieReelle;

    private java.lang.String idMedecinResponsable;

    private java.lang.String idMedecinTraitant;

    private java.lang.Integer ippSigemsAssure;

    private java.lang.Integer ippSigemsPatient;

    private java.lang.String litSouhaite;

    private java.lang.String modeTraitement;

    private java.lang.String motif;

    private java.lang.String numero;

    private java.lang.String numeroPA;

    private sigemsreeduc.dosbase.WsPersonne personneDeConfiance;

    private sigemsreeduc.dosbase.WsPersonne[] personnesAPrevenir;

    private java.lang.String service;

    private java.lang.String typeChambreSouhaite;

    public WsDossierBase() {
    }

    @Override
    public String toString() {
        return "WsDossierBase{" +
                "codeMedecinResponsable='" + codeMedecinResponsable + '\'' +
                ", codeMedecinTraitant='" + codeMedecinTraitant + '\'' +
                ", dateEntree='" + dateEntree + '\'' +
                ", dateSortiePrevue='" + dateSortiePrevue + '\'' +
                ", dateSortieReelle='" + dateSortieReelle + '\'' +
                ", heureEntree='" + heureEntree + '\'' +
                ", heureSortiePrevue='" + heureSortiePrevue + '\'' +
                ", heureSortieReelle='" + heureSortieReelle + '\'' +
                ", idMedecinResponsable='" + idMedecinResponsable + '\'' +
                ", idMedecinTraitant='" + idMedecinTraitant + '\'' +
                ", ippSigemsAssure=" + ippSigemsAssure +
                ", ippSigemsPatient=" + ippSigemsPatient +
                ", litSouhaite='" + litSouhaite + '\'' +
                ", modeTraitement='" + modeTraitement + '\'' +
                ", motif='" + motif + '\'' +
                ", numero='" + numero + '\'' +
                ", numeroPA='" + numeroPA + '\'' +
                ", personneDeConfiance=" + personneDeConfiance +
                ", personnesAPrevenir=" + Arrays.toString(personnesAPrevenir) +
                ", service='" + service + '\'' +
                ", typeChambreSouhaite='" + typeChambreSouhaite + '\'' +
                ", __equalsCalc=" + __equalsCalc +
                ", __hashCodeCalc=" + __hashCodeCalc +
                '}';
    }

    public WsDossierBase(
           java.lang.String codeMedecinResponsable,
           java.lang.String codeMedecinTraitant,
           java.lang.String dateEntree,
           java.lang.String dateSortiePrevue,
           java.lang.String dateSortieReelle,
           java.lang.String heureEntree,
           java.lang.String heureSortiePrevue,
           java.lang.String heureSortieReelle,
           java.lang.String idMedecinResponsable,
           java.lang.String idMedecinTraitant,
           java.lang.Integer ippSigemsAssure,
           java.lang.Integer ippSigemsPatient,
           java.lang.String litSouhaite,
           java.lang.String modeTraitement,
           java.lang.String motif,
           java.lang.String numero,
           java.lang.String numeroPA,
           sigemsreeduc.dosbase.WsPersonne personneDeConfiance,
           sigemsreeduc.dosbase.WsPersonne[] personnesAPrevenir,
           java.lang.String service,
           java.lang.String typeChambreSouhaite) {
           this.codeMedecinResponsable = codeMedecinResponsable;
           this.codeMedecinTraitant = codeMedecinTraitant;
           this.dateEntree = dateEntree;
           this.dateSortiePrevue = dateSortiePrevue;
           this.dateSortieReelle = dateSortieReelle;
           this.heureEntree = heureEntree;
           this.heureSortiePrevue = heureSortiePrevue;
           this.heureSortieReelle = heureSortieReelle;
           this.idMedecinResponsable = idMedecinResponsable;
           this.idMedecinTraitant = idMedecinTraitant;
           this.ippSigemsAssure = ippSigemsAssure;
           this.ippSigemsPatient = ippSigemsPatient;
           this.litSouhaite = litSouhaite;
           this.modeTraitement = modeTraitement;
           this.motif = motif;
           this.numero = numero;
           this.numeroPA = numeroPA;
           this.personneDeConfiance = personneDeConfiance;
           this.personnesAPrevenir = personnesAPrevenir;
           this.service = service;
           this.typeChambreSouhaite = typeChambreSouhaite;
    }


    /**
     * Gets the codeMedecinResponsable value for this WsDossierBase.
     * 
     * @return codeMedecinResponsable
     */
    public java.lang.String getCodeMedecinResponsable() {
        return codeMedecinResponsable;
    }


    /**
     * Sets the codeMedecinResponsable value for this WsDossierBase.
     * 
     * @param codeMedecinResponsable
     */
    public void setCodeMedecinResponsable(java.lang.String codeMedecinResponsable) {
        this.codeMedecinResponsable = codeMedecinResponsable;
    }


    /**
     * Gets the codeMedecinTraitant value for this WsDossierBase.
     * 
     * @return codeMedecinTraitant
     */
    public java.lang.String getCodeMedecinTraitant() {
        return codeMedecinTraitant;
    }


    /**
     * Sets the codeMedecinTraitant value for this WsDossierBase.
     * 
     * @param codeMedecinTraitant
     */
    public void setCodeMedecinTraitant(java.lang.String codeMedecinTraitant) {
        this.codeMedecinTraitant = codeMedecinTraitant;
    }


    /**
     * Gets the dateEntree value for this WsDossierBase.
     * 
     * @return dateEntree
     */
    public java.lang.String getDateEntree() {
        return dateEntree;
    }


    /**
     * Sets the dateEntree value for this WsDossierBase.
     * 
     * @param dateEntree
     */
    public void setDateEntree(java.lang.String dateEntree) {
        this.dateEntree = dateEntree;
    }


    /**
     * Gets the dateSortiePrevue value for this WsDossierBase.
     * 
     * @return dateSortiePrevue
     */
    public java.lang.String getDateSortiePrevue() {
        return dateSortiePrevue;
    }


    /**
     * Sets the dateSortiePrevue value for this WsDossierBase.
     * 
     * @param dateSortiePrevue
     */
    public void setDateSortiePrevue(java.lang.String dateSortiePrevue) {
        this.dateSortiePrevue = dateSortiePrevue;
    }


    /**
     * Gets the dateSortieReelle value for this WsDossierBase.
     * 
     * @return dateSortieReelle
     */
    public java.lang.String getDateSortieReelle() {
        return dateSortieReelle;
    }


    /**
     * Sets the dateSortieReelle value for this WsDossierBase.
     * 
     * @param dateSortieReelle
     */
    public void setDateSortieReelle(java.lang.String dateSortieReelle) {
        this.dateSortieReelle = dateSortieReelle;
    }


    /**
     * Gets the heureEntree value for this WsDossierBase.
     * 
     * @return heureEntree
     */
    public java.lang.String getHeureEntree() {
        return heureEntree;
    }


    /**
     * Sets the heureEntree value for this WsDossierBase.
     * 
     * @param heureEntree
     */
    public void setHeureEntree(java.lang.String heureEntree) {
        this.heureEntree = heureEntree;
    }


    /**
     * Gets the heureSortiePrevue value for this WsDossierBase.
     * 
     * @return heureSortiePrevue
     */
    public java.lang.String getHeureSortiePrevue() {
        return heureSortiePrevue;
    }


    /**
     * Sets the heureSortiePrevue value for this WsDossierBase.
     * 
     * @param heureSortiePrevue
     */
    public void setHeureSortiePrevue(java.lang.String heureSortiePrevue) {
        this.heureSortiePrevue = heureSortiePrevue;
    }


    /**
     * Gets the heureSortieReelle value for this WsDossierBase.
     * 
     * @return heureSortieReelle
     */
    public java.lang.String getHeureSortieReelle() {
        return heureSortieReelle;
    }


    /**
     * Sets the heureSortieReelle value for this WsDossierBase.
     * 
     * @param heureSortieReelle
     */
    public void setHeureSortieReelle(java.lang.String heureSortieReelle) {
        this.heureSortieReelle = heureSortieReelle;
    }


    /**
     * Gets the idMedecinResponsable value for this WsDossierBase.
     * 
     * @return idMedecinResponsable
     */
    public java.lang.String getIdMedecinResponsable() {
        return idMedecinResponsable;
    }


    /**
     * Sets the idMedecinResponsable value for this WsDossierBase.
     * 
     * @param idMedecinResponsable
     */
    public void setIdMedecinResponsable(java.lang.String idMedecinResponsable) {
        this.idMedecinResponsable = idMedecinResponsable;
    }


    /**
     * Gets the idMedecinTraitant value for this WsDossierBase.
     * 
     * @return idMedecinTraitant
     */
    public java.lang.String getIdMedecinTraitant() {
        return idMedecinTraitant;
    }


    /**
     * Sets the idMedecinTraitant value for this WsDossierBase.
     * 
     * @param idMedecinTraitant
     */
    public void setIdMedecinTraitant(java.lang.String idMedecinTraitant) {
        this.idMedecinTraitant = idMedecinTraitant;
    }


    /**
     * Gets the ippSigemsAssure value for this WsDossierBase.
     * 
     * @return ippSigemsAssure
     */
    public java.lang.Integer getIppSigemsAssure() {
        return ippSigemsAssure;
    }


    /**
     * Sets the ippSigemsAssure value for this WsDossierBase.
     * 
     * @param ippSigemsAssure
     */
    public void setIppSigemsAssure(java.lang.Integer ippSigemsAssure) {
        this.ippSigemsAssure = ippSigemsAssure;
    }


    /**
     * Gets the ippSigemsPatient value for this WsDossierBase.
     * 
     * @return ippSigemsPatient
     */
    public java.lang.Integer getIppSigemsPatient() {
        return ippSigemsPatient;
    }


    /**
     * Sets the ippSigemsPatient value for this WsDossierBase.
     * 
     * @param ippSigemsPatient
     */
    public void setIppSigemsPatient(java.lang.Integer ippSigemsPatient) {
        this.ippSigemsPatient = ippSigemsPatient;
    }


    /**
     * Gets the litSouhaite value for this WsDossierBase.
     * 
     * @return litSouhaite
     */
    public java.lang.String getLitSouhaite() {
        return litSouhaite;
    }


    /**
     * Sets the litSouhaite value for this WsDossierBase.
     * 
     * @param litSouhaite
     */
    public void setLitSouhaite(java.lang.String litSouhaite) {
        this.litSouhaite = litSouhaite;
    }


    /**
     * Gets the modeTraitement value for this WsDossierBase.
     * 
     * @return modeTraitement
     */
    public java.lang.String getModeTraitement() {
        return modeTraitement;
    }


    /**
     * Sets the modeTraitement value for this WsDossierBase.
     * 
     * @param modeTraitement
     */
    public void setModeTraitement(java.lang.String modeTraitement) {
        this.modeTraitement = modeTraitement;
    }


    /**
     * Gets the motif value for this WsDossierBase.
     * 
     * @return motif
     */
    public java.lang.String getMotif() {
        return motif;
    }


    /**
     * Sets the motif value for this WsDossierBase.
     * 
     * @param motif
     */
    public void setMotif(java.lang.String motif) {
        this.motif = motif;
    }


    /**
     * Gets the numero value for this WsDossierBase.
     * 
     * @return numero
     */
    public java.lang.String getNumero() {
        return numero;
    }


    /**
     * Sets the numero value for this WsDossierBase.
     * 
     * @param numero
     */
    public void setNumero(java.lang.String numero) {
        this.numero = numero;
    }


    /**
     * Gets the numeroPA value for this WsDossierBase.
     * 
     * @return numeroPA
     */
    public java.lang.String getNumeroPA() {
        return numeroPA;
    }


    /**
     * Sets the numeroPA value for this WsDossierBase.
     * 
     * @param numeroPA
     */
    public void setNumeroPA(java.lang.String numeroPA) {
        this.numeroPA = numeroPA;
    }


    /**
     * Gets the personneDeConfiance value for this WsDossierBase.
     * 
     * @return personneDeConfiance
     */
    public sigemsreeduc.dosbase.WsPersonne getPersonneDeConfiance() {
        return personneDeConfiance;
    }


    /**
     * Sets the personneDeConfiance value for this WsDossierBase.
     * 
     * @param personneDeConfiance
     */
    public void setPersonneDeConfiance(sigemsreeduc.dosbase.WsPersonne personneDeConfiance) {
        this.personneDeConfiance = personneDeConfiance;
    }


    /**
     * Gets the personnesAPrevenir value for this WsDossierBase.
     * 
     * @return personnesAPrevenir
     */
    public sigemsreeduc.dosbase.WsPersonne[] getPersonnesAPrevenir() {
        return personnesAPrevenir;
    }


    /**
     * Sets the personnesAPrevenir value for this WsDossierBase.
     * 
     * @param personnesAPrevenir
     */
    public void setPersonnesAPrevenir(sigemsreeduc.dosbase.WsPersonne[] personnesAPrevenir) {
        this.personnesAPrevenir = personnesAPrevenir;
    }


    /**
     * Gets the service value for this WsDossierBase.
     * 
     * @return service
     */
    public java.lang.String getService() {
        return service;
    }


    /**
     * Sets the service value for this WsDossierBase.
     * 
     * @param service
     */
    public void setService(java.lang.String service) {
        this.service = service;
    }


    /**
     * Gets the typeChambreSouhaite value for this WsDossierBase.
     * 
     * @return typeChambreSouhaite
     */
    public java.lang.String getTypeChambreSouhaite() {
        return typeChambreSouhaite;
    }


    /**
     * Sets the typeChambreSouhaite value for this WsDossierBase.
     * 
     * @param typeChambreSouhaite
     */
    public void setTypeChambreSouhaite(java.lang.String typeChambreSouhaite) {
        this.typeChambreSouhaite = typeChambreSouhaite;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof WsDossierBase)) return false;
        WsDossierBase other = (WsDossierBase) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codeMedecinResponsable==null && other.getCodeMedecinResponsable()==null) || 
             (this.codeMedecinResponsable!=null &&
              this.codeMedecinResponsable.equals(other.getCodeMedecinResponsable()))) &&
            ((this.codeMedecinTraitant==null && other.getCodeMedecinTraitant()==null) || 
             (this.codeMedecinTraitant!=null &&
              this.codeMedecinTraitant.equals(other.getCodeMedecinTraitant()))) &&
            ((this.dateEntree==null && other.getDateEntree()==null) || 
             (this.dateEntree!=null &&
              this.dateEntree.equals(other.getDateEntree()))) &&
            ((this.dateSortiePrevue==null && other.getDateSortiePrevue()==null) || 
             (this.dateSortiePrevue!=null &&
              this.dateSortiePrevue.equals(other.getDateSortiePrevue()))) &&
            ((this.dateSortieReelle==null && other.getDateSortieReelle()==null) || 
             (this.dateSortieReelle!=null &&
              this.dateSortieReelle.equals(other.getDateSortieReelle()))) &&
            ((this.heureEntree==null && other.getHeureEntree()==null) || 
             (this.heureEntree!=null &&
              this.heureEntree.equals(other.getHeureEntree()))) &&
            ((this.heureSortiePrevue==null && other.getHeureSortiePrevue()==null) || 
             (this.heureSortiePrevue!=null &&
              this.heureSortiePrevue.equals(other.getHeureSortiePrevue()))) &&
            ((this.heureSortieReelle==null && other.getHeureSortieReelle()==null) || 
             (this.heureSortieReelle!=null &&
              this.heureSortieReelle.equals(other.getHeureSortieReelle()))) &&
            ((this.idMedecinResponsable==null && other.getIdMedecinResponsable()==null) || 
             (this.idMedecinResponsable!=null &&
              this.idMedecinResponsable.equals(other.getIdMedecinResponsable()))) &&
            ((this.idMedecinTraitant==null && other.getIdMedecinTraitant()==null) || 
             (this.idMedecinTraitant!=null &&
              this.idMedecinTraitant.equals(other.getIdMedecinTraitant()))) &&
            ((this.ippSigemsAssure==null && other.getIppSigemsAssure()==null) || 
             (this.ippSigemsAssure!=null &&
              this.ippSigemsAssure.equals(other.getIppSigemsAssure()))) &&
            ((this.ippSigemsPatient==null && other.getIppSigemsPatient()==null) || 
             (this.ippSigemsPatient!=null &&
              this.ippSigemsPatient.equals(other.getIppSigemsPatient()))) &&
            ((this.litSouhaite==null && other.getLitSouhaite()==null) || 
             (this.litSouhaite!=null &&
              this.litSouhaite.equals(other.getLitSouhaite()))) &&
            ((this.modeTraitement==null && other.getModeTraitement()==null) || 
             (this.modeTraitement!=null &&
              this.modeTraitement.equals(other.getModeTraitement()))) &&
            ((this.motif==null && other.getMotif()==null) || 
             (this.motif!=null &&
              this.motif.equals(other.getMotif()))) &&
            ((this.numero==null && other.getNumero()==null) || 
             (this.numero!=null &&
              this.numero.equals(other.getNumero()))) &&
            ((this.numeroPA==null && other.getNumeroPA()==null) || 
             (this.numeroPA!=null &&
              this.numeroPA.equals(other.getNumeroPA()))) &&
            ((this.personneDeConfiance==null && other.getPersonneDeConfiance()==null) || 
             (this.personneDeConfiance!=null &&
              this.personneDeConfiance.equals(other.getPersonneDeConfiance()))) &&
            ((this.personnesAPrevenir==null && other.getPersonnesAPrevenir()==null) || 
             (this.personnesAPrevenir!=null &&
              java.util.Arrays.equals(this.personnesAPrevenir, other.getPersonnesAPrevenir()))) &&
            ((this.service==null && other.getService()==null) || 
             (this.service!=null &&
              this.service.equals(other.getService()))) &&
            ((this.typeChambreSouhaite==null && other.getTypeChambreSouhaite()==null) || 
             (this.typeChambreSouhaite!=null &&
              this.typeChambreSouhaite.equals(other.getTypeChambreSouhaite())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodeMedecinResponsable() != null) {
            _hashCode += getCodeMedecinResponsable().hashCode();
        }
        if (getCodeMedecinTraitant() != null) {
            _hashCode += getCodeMedecinTraitant().hashCode();
        }
        if (getDateEntree() != null) {
            _hashCode += getDateEntree().hashCode();
        }
        if (getDateSortiePrevue() != null) {
            _hashCode += getDateSortiePrevue().hashCode();
        }
        if (getDateSortieReelle() != null) {
            _hashCode += getDateSortieReelle().hashCode();
        }
        if (getHeureEntree() != null) {
            _hashCode += getHeureEntree().hashCode();
        }
        if (getHeureSortiePrevue() != null) {
            _hashCode += getHeureSortiePrevue().hashCode();
        }
        if (getHeureSortieReelle() != null) {
            _hashCode += getHeureSortieReelle().hashCode();
        }
        if (getIdMedecinResponsable() != null) {
            _hashCode += getIdMedecinResponsable().hashCode();
        }
        if (getIdMedecinTraitant() != null) {
            _hashCode += getIdMedecinTraitant().hashCode();
        }
        if (getIppSigemsAssure() != null) {
            _hashCode += getIppSigemsAssure().hashCode();
        }
        if (getIppSigemsPatient() != null) {
            _hashCode += getIppSigemsPatient().hashCode();
        }
        if (getLitSouhaite() != null) {
            _hashCode += getLitSouhaite().hashCode();
        }
        if (getModeTraitement() != null) {
            _hashCode += getModeTraitement().hashCode();
        }
        if (getMotif() != null) {
            _hashCode += getMotif().hashCode();
        }
        if (getNumero() != null) {
            _hashCode += getNumero().hashCode();
        }
        if (getNumeroPA() != null) {
            _hashCode += getNumeroPA().hashCode();
        }
        if (getPersonneDeConfiance() != null) {
            _hashCode += getPersonneDeConfiance().hashCode();
        }
        if (getPersonnesAPrevenir() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPersonnesAPrevenir());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPersonnesAPrevenir(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getService() != null) {
            _hashCode += getService().hashCode();
        }
        if (getTypeChambreSouhaite() != null) {
            _hashCode += getTypeChambreSouhaite().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WsDossierBase.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:SigeGateDosBase", "WsDossierBase"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codeMedecinResponsable");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codeMedecinResponsable"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codeMedecinTraitant");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codeMedecinTraitant"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateEntree");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dateEntree"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateSortiePrevue");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dateSortiePrevue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateSortieReelle");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dateSortieReelle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("heureEntree");
        elemField.setXmlName(new javax.xml.namespace.QName("", "heureEntree"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("heureSortiePrevue");
        elemField.setXmlName(new javax.xml.namespace.QName("", "heureSortiePrevue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("heureSortieReelle");
        elemField.setXmlName(new javax.xml.namespace.QName("", "heureSortieReelle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idMedecinResponsable");
        elemField.setXmlName(new javax.xml.namespace.QName("", "idMedecinResponsable"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idMedecinTraitant");
        elemField.setXmlName(new javax.xml.namespace.QName("", "idMedecinTraitant"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ippSigemsAssure");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ippSigemsAssure"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ippSigemsPatient");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ippSigemsPatient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("litSouhaite");
        elemField.setXmlName(new javax.xml.namespace.QName("", "litSouhaite"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modeTraitement");
        elemField.setXmlName(new javax.xml.namespace.QName("", "modeTraitement"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("motif");
        elemField.setXmlName(new javax.xml.namespace.QName("", "motif"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numero");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numero"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numeroPA");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numeroPA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("personneDeConfiance");
        elemField.setXmlName(new javax.xml.namespace.QName("", "personneDeConfiance"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:SigeGateDosBase", "WsPersonne"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("personnesAPrevenir");
        elemField.setXmlName(new javax.xml.namespace.QName("", "personnesAPrevenir"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:SigeGateDosBase", "WsPersonne"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("service");
        elemField.setXmlName(new javax.xml.namespace.QName("", "service"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("typeChambreSouhaite");
        elemField.setXmlName(new javax.xml.namespace.QName("", "typeChambreSouhaite"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
