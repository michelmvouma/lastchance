/**
 * SigeGateDosBase_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package sigemsreeduc.dosbase;

public interface SigeGateDosBase_PortType extends java.rmi.Remote {
    public java.lang.String selectDossier(java.lang.String idBase, java.lang.String numero, sigemsreeduc.dosbase.holders.WsDossierBaseHolder dossier) throws java.rmi.RemoteException;
    public java.lang.String searchDossier(java.lang.String idBase, java.lang.Integer page, java.lang.Integer nbLigne, sigemsreeduc.dosbase.WsDossierBase dossier, java.lang.String dateEntreeInf, java.lang.String dateEntreeSup, sigemsreeduc.dosbase.holders.WsDossierBaseListeHolder listeDossiers) throws java.rmi.RemoteException;
    public java.lang.String createPA(java.lang.String idBase, sigemsreeduc.dosbase.WsDossierBase dossier, javax.xml.rpc.holders.StringHolder numero) throws java.rmi.RemoteException;
    public java.lang.String updatePA(java.lang.String idBase, sigemsreeduc.dosbase.WsDossierBase dossier) throws java.rmi.RemoteException;
    public java.lang.String updateDossier(java.lang.String idBase, sigemsreeduc.dosbase.WsDossierBase dossier) throws java.rmi.RemoteException;
    public java.lang.String deletePA(java.lang.String idBase, sigemsreeduc.dosbase.WsDossierBase dossier) throws java.rmi.RemoteException;
    public java.lang.String selectDossierModifie(java.lang.String idBase, java.lang.Integer page, java.lang.Integer nbLigne, java.lang.String dateInf, java.lang.String heureInf, java.lang.String dateSup, java.lang.String heureSup, sigemsreeduc.dosbase.holders.WsDossierBaseListeHolder listeDossiers) throws java.rmi.RemoteException;
    public java.lang.String selectDossierSupprime(java.lang.String idBase, java.lang.String date, sigemsreeduc.dosbase.holders.StringListeHolder listeNumerosDossiers) throws java.rmi.RemoteException;
    public java.lang.String ajouterPersonneDeConfiance(java.lang.String idBase, java.lang.String numero, sigemsreeduc.dosbase.WsPersonne personne) throws java.rmi.RemoteException;
    public java.lang.String ajouterPersonnesAPrevenir(java.lang.String idBase, java.lang.String numero, sigemsreeduc.dosbase.WsPersonneListe listePersonnes) throws java.rmi.RemoteException;
    public java.lang.String supprimerPersonneDeConfiance(java.lang.String idBase, java.lang.String numero) throws java.rmi.RemoteException;
    public java.lang.String supprimerPersonnesAPrevenir(java.lang.String idBase, java.lang.String numero) throws java.rmi.RemoteException;
}
