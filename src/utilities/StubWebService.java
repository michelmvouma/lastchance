package utilities;



import entities.DataImportSigems;
import sigemsreeduc.acte.SigeGateActeNgapServiceLocator;
import sigemsreeduc.acte.SigeGateActeNgapSoapBindingStub;
import sigemsreeduc.acte.WsActeNgap;
import sigemsreeduc.acte.WsActeNgapListe;
import sigemsreeduc.acte.holders.WsActeNgapListeHolder;
import sigemsreeduc.dosbase.SigeGateDosBaseServiceLocator;
import sigemsreeduc.dosbase.SigeGateDosBaseSoapBindingStub;
import sigemsreeduc.dosbase.WsDossierBase;
import sigemsreeduc.dosbase.holders.WsDossierBaseHolder;
import sigemsreeduc.sigegatepat.SigeGatePatServiceLocator;
import sigemsreeduc.sigegatepat.SigeGatePatSoapBindingStub;
import sigemsreeduc.sigegatepat.WsPatient;
import sigemsreeduc.sigegatepat.holders.WsPatientHolder;
import sigemsreeduc.webServiceWindev.IatReedWebServiceLocator;
import sigemsreeduc.webServiceWindev.IatReedWebServiceSOAPBindingStub;

import javax.xml.rpc.ServiceException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by mmvouma on 03/04/2017.
 */
public class StubWebService {
   private StubWebService() {
    }
   private static StubWebService instance = null;
   private int endFirstIndex = 2;
   private static final String BASEID="bddc_2stb";
   private static final int NBRELIGNE =50;

    public static int getNBRELIGNE() {
        return NBRELIGNE;
    }

    public static String getBASEID() {
        return BASEID;
    }

    /**
     * le binding ici il s'agit du stub du service sigegateDoseBase
     */
   private static SigeGateDosBaseSoapBindingStub binding = null;
   private static SigeGateActeNgapSoapBindingStub bindingSigeGateActeNgap =null;
    private static SigeGatePatSoapBindingStub bindingSigeGatePat =null;
   private static final String PASSWORD_WEB = "Fp82E1b";
    private static final String USERNAME_WEB = "ATsgmsSTB";



    /**
     *
     * @return StubWebService
     */
   public static StubWebService getInstance()
   {
       if(instance==null)
       {
           instance = new StubWebService();
       }
       return instance;
   }
    /**
     * Obtention du stub statiquement
     * @return
     */
    public static SigeGateDosBaseSoapBindingStub getBinding() {
       if(binding==null)
       {
           try {
               binding = (SigeGateDosBaseSoapBindingStub)new SigeGateDosBaseServiceLocator().getSigeGateDosBase();
               binding.setUsername(USERNAME_WEB); //AdminIAT
               binding.setPassword(PASSWORD_WEB); //Inter0p
               System.out.println("binding SigeGateDoseBase réussie");
           }
           catch (javax.xml.rpc.ServiceException jre)
           {
               System.out.println("erreur");
           }
       }
        return binding;
    }

    /**
     * Obtention du Stub SigeGateActeNgap
     * @return
     */
    public static SigeGateActeNgapSoapBindingStub getBindingStubSigeGateActeNgap()
    {
       if(bindingSigeGateActeNgap==null)
       {
           try {
               bindingSigeGateActeNgap = (SigeGateActeNgapSoapBindingStub) new SigeGateActeNgapServiceLocator().getSigeGateActeNgap();
               bindingSigeGateActeNgap.setUsername(USERNAME_WEB); //AdminIAT
               bindingSigeGateActeNgap.setPassword(PASSWORD_WEB);
           } catch (ServiceException e) {
               System.out.println("Erreur impossible d'avoir le stub sigeGateActeNgap " +e.getMessage() + " "+e.getLocalizedMessage());
               //e.printStackTrace();
           }
       }
       return bindingSigeGateActeNgap;
    }

    /**
     * renvoi le stub pour la gestion des patients
     * @return
     */
    public static SigeGatePatSoapBindingStub getBindingStubSigeGatePat()
    {
        if(bindingSigeGatePat==null)
        {
            try {
                bindingSigeGatePat = (SigeGatePatSoapBindingStub) new SigeGatePatServiceLocator().getSigeGatePat();
                bindingSigeGatePat.setUsername(USERNAME_WEB); //AdminIAT
                bindingSigeGatePat.setPassword(PASSWORD_WEB);
            } catch (ServiceException e) {
                System.out.println("Erreur impossible d'avoir le stub sigeGatePat " +e.getMessage() + " "+e.getLocalizedMessage());
                //e.printStackTrace();
            }
        }
        return bindingSigeGatePat;
    }



    /**
     * important ippSigemsP = identifiant du patient
     * @param numeroDossier
     * @param siGateDosBaseStub
     * @param bindingSigeGatePat
     * @return
     */
    public DataImportSigems informationPatAndFolderMD(String numeroDossier, SigeGateDosBaseSoapBindingStub siGateDosBaseStub, SigeGatePatSoapBindingStub bindingSigeGatePat)
    {
        String idBase = getBASEID();
        WsDossierBaseHolder listeDoc = new WsDossierBaseHolder();
        WsPatientHolder patFind =null;
        DataImportSigems dataImportSigems = new DataImportSigems();

        try {
            String value = siGateDosBaseStub.selectDossier(idBase ,numeroDossier , listeDoc );
            if(value.equals("0") && listeDoc!=null && listeDoc.getValue().getIppSigemsPatient()!=null)
            {
              patFind = new WsPatientHolder();
              int numeroPat = listeDoc.getValue().getIppSigemsPatient();
                WsDossierBase dossierPat = listeDoc.getValue();

                //*System.out.println("Dossier patient " + dossierPat);

                /**
                 * Qelque information sur le dossier
                 */
                dataImportSigems.setIdMedecinResponsable(dossierPat.getIdMedecinResponsable());
                dataImportSigems.setCodeMedecinResponsable(dossierPat.getCodeMedecinResponsable());
                dataImportSigems.setIdMedecinTraitant(dossierPat.getIdMedecinTraitant());
                dataImportSigems.setCodeMedecinTraitant(dossierPat.getCodeMedecinTraitant());
                dataImportSigems.setNumeroPA(dossierPat.getNumeroPA());

                dataImportSigems.setServ(dossierPat.getService());
                if(dossierPat.getNumero()!=null && dossierPat.getNumero().length()>=endFirstIndex)
                {
                    String annee = dossierPat.getNumero().substring(0, endFirstIndex);
                    String code = dossierPat.getNumero().substring(endFirstIndex, dossierPat.getNumero().length());

                    dataImportSigems.setAnnee(annee);
                    dataImportSigems.setCode(code);
                }

                /**
                 * Recherche des informations sur le patient
                 */
                value = bindingSigeGatePat.selectPatient(idBase, numeroPat, patFind); //21780454
                if(patFind!=null && patFind.getValue()!=null)
                {
                    WsPatient patient = patFind.getValue();
                    dataImportSigems.setNomNaissance(patient.getNomNaissance());
                    dataImportSigems.setNomUsuel(patient.getNomUsuel());
                    dataImportSigems.setPrenom(patient.getPrenom());
                    //dataImportSigems.setNumeroPA(patient.getN);
                    //*System.out.println("information patient " + patFind.getValue().toString());

                }
            }
        } catch (RemoteException e) {
            System.out.println("ERREUR " + StubWebService.class.getName() +" Methode informationPatAndFolderMD " + e.getMessage());
            e.printStackTrace();
        }
        return dataImportSigems;
    }

    /**
     * Méthode coeur du metier du programme
     * Méthode permettant de rechercher la liste des actes entre deux dates
     * @param dateDebut date de debut pour la recherche
     * @param dateFin date de fin pour la recherche
     * @return
     */
    public List<DataImportSigems> getDataActesEntreDeuxDates(String dateDebut, String dateFin)
    {
        WsActeNgapListeHolder listeResultatActe = new WsActeNgapListeHolder();
        WsActeNgapListeHolder listeResultatActeCopie = new WsActeNgapListeHolder();
        List<DataImportSigems> dataToImport = new ArrayList<>();
        List<WsActeNgap> listActeSigems = new ArrayList<>();

        int tailleTotale = 0;
        int pageFind = 1;
        int pageCount = 0;
        int nbreLigne = 0;
        int nbreLigneIncrement = 50;
        WsActeNgap wsActe = new WsActeNgap();
        boolean loop = true;
        String idBase = StubWebService.getBASEID();
        SigeGateActeNgapSoapBindingStub bindingSigeGateActeNgap = StubWebService.getBindingStubSigeGateActeNgap();
        if((dateDebut!=null && dateDebut.length()>0) && (dateFin!=null && dateFin.length()>0))
        {
            while(loop)
            {
                try {
                    listeResultatActeCopie.setValue(new WsActeNgapListe());
                    //04/01/2017 dateDebut
                    // 05/01/2017 dateFin
                    bindingSigeGateActeNgap.searchActe(idBase, pageFind, StubWebService.getNBRELIGNE() , wsActe, dateDebut, dateFin, listeResultatActeCopie);
                    if(listeResultatActeCopie.getValue()!=null)
                    {
                        listeResultatActe.setValue(listeResultatActeCopie.getValue());
                    }else{
                        listeResultatActe.setValue(null);
                    }
                    if(listeResultatActe!=null && listeResultatActe.getValue()!=null)
                    {
                        if(listeResultatActe.getValue().getListe()!=null && listeResultatActe.getValue().getListe().length>0)
                        {
                            pageFind++;
                            nbreLigne +=listeResultatActe.getValue().getListe().length;
                            pageCount++;

                            listActeSigems = new ArrayList<>(Arrays.asList(listeResultatActe.getValue().getListe()));
                            for (WsActeNgap acte : listActeSigems)
                            {
                                // si l'acte fait partie de la confirmation alors on l'ajoute
                                // dans la liste des actes
                                if(confirmationLettreCle(acte.getLettreCle()))
                                {
                                    if(acte.getNumDossier()!=null)
                                    {
                                        //appelle methode informationPatientPatAndFoelder
                                        DataImportSigems dataImportSigems = this.informationPatAndFolderMD(acte.getNumDossier(), binding, bindingSigeGatePat);
                                        dataImportSigems.setDateActeHono(acte.getExecuteDate());
                                        dataImportSigems.setNumDossier(acte.getNumDossier());
                                        try{
                                            if(acte.getExecuteDate()!=null)
                                            {
                                                dataImportSigems.setCoeffHonor(String.valueOf(acte.getCoefficient()));
                                            }
                                        }catch(Exception e)
                                        {
                                            dataImportSigems.setCoeffHonor("");
                                        }
                                        dataImportSigems.setLettreCle(acte.getLettreCle());
                                        dataImportSigems.setQuantite(acte.getQuantite());
                                        dataImportSigems.setAdeliPratExec(acte.getAdeliPratExec());//specialite
                                        dataImportSigems.setCodePratExec(acte.getCodePratExec()); //specialite
                                        dataToImport.add(dataImportSigems);
                                    }
                                }

                            }// fin de la boucle de parcours des actes de l'utilisateur
                        }else{
                            loop = false;
                        }
                    }else{
                        loop = false;
                    }
                    listeResultatActe.setValue(new WsActeNgapListe());
                } catch (RemoteException e) {
                    loop = false;
                    /*System.out.println("ERREUR "+StubWebService.class.getName() + " METHODE getDataActesEntreDeuxDates "
                            +e.getMessage() + " Localisation "+e.getLocalizedMessage());*/
                    //e.printStackTrace();
                }
            }
        }
        return dataToImport;
    }

    /**
     * formatage des données pour envoyer au web service
     * @param message
     * @return
     */
    public static String formatData(String message)
    {
        message = message.replace("\"", "'");
        if(message.length()>0)
        {
            message = message.replace("},", ";").replace("[", "").replace("]", "").replace("{", "").replace("}","").trim();
            int lastIndex = message.lastIndexOf(";");
            //System.out.println("last index "+ lastIndex + " taille " + message.length());
        }
        return message;
    }

    /**
     *
     * @param lettreCle
     * @return
     */
    public Boolean confirmationLettreCle(String lettreCle)
    {
        boolean resultat = false;
        //System.out.println("CONFIRMATION LETTRE CLE "+ lettreCle);
        if(lettreCle!=null && lettreCle.length()>0)
        {
            lettreCle = lettreCle.trim().toLowerCase();
            switch (lettreCle)
            {
                case "amcg":
                    resultat = true;
                    break;

                case "echg": // a regarder soit h soit a
                resultat = true;
                break;

                case "eleg":
                    resultat = true;
                    break;

                case "epng": // a regarder soit r soit n
                    resultat = true;
                    break;
                case "reeg":
                    resultat = true;
                    break;
            }
        }


        return  resultat;
    }

    /**
     * Appelle méthode à distance vers le stub
     * @param dataAsauvegarder
     * @param nbreElement
     * @param typeEtab
     * @param nomEtab
     * @param dateDebut
     * @param dateFin
     * @return
     */
    public String  executeSaveDataIntoWebService(String dataAsauvegarder, int nbreElement, String typeEtab, String nomEtab, String dateDebut, String dateFin)
    {
        IatReedWebServiceSOAPBindingStub binding;
        String resultatInsertion ="";
        try {
            binding = (IatReedWebServiceSOAPBindingStub)new IatReedWebServiceLocator().getiatReedWebServiceSOAPPort();
            resultatInsertion = binding.insertionDonneesDansLesTables(dataAsauvegarder, nbreElement, typeEtab, nomEtab, dateDebut, dateFin);
        } catch (ServiceException e) {
            System.out.println( "ERREUR CREATION STUB  " + e.getMessage());
            e.printStackTrace();
        } catch (RemoteException remote) {
            System.out.println( "REMOTE EXCEPTION ERREUR APPEL PROCEDURE A DISTANCE " + remote.getMessage());
            remote.printStackTrace();
        }

        return resultatInsertion;
    }


}
