package entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by mmvouma on 03/04/2017.
 */
@Entity
public class DataImportSigems implements Serializable{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String dateActeHono;

    private String code;

    private String annee;

    private String pratHonor;

    private String specialite;

    private String prodHonor;

    private String coeffHonor;

    private String serv;


    private int ippSigems ;
    private String nomNaissance ;
    private String nomUsuel;

    public int getIppSigems() {
        return ippSigems;
    }

    public void setIppSigems(int ippSigems) {
        this.ippSigems = ippSigems;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getNomNaissance() {
        return nomNaissance;
    }

    public void setNomNaissance(String nomNaissance) {
        this.nomNaissance = nomNaissance;
    }

    public String getNomUsuel() {
        return nomUsuel;
    }

    public void setNomUsuel(String nomUsuel) {
        this.nomUsuel = nomUsuel;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNumeroPA() {
        return numeroPA;
    }

    public void setNumeroPA(String numeroPA) {
        this.numeroPA = numeroPA;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    private String prenom;
    private String numeroPA;
    private String numero;
    private String idMedecinResponsable;
    private String idMedecinTraitant;
    private String codeMedecinResponsable;
    private String codeMedecinTraitant;
    private String lettreCle;
    private Float quantite;
    private String numDossier;
    private String codePratExec;
    private String adeliPratExec;

    public String getNumDossier() {
        return numDossier;
    }

    public void setNumDossier(String numDossier) {
        this.numDossier = numDossier;
    }

    public String getLettreCle() {
        return lettreCle;
    }

    public void setLettreCle(String lettreCle) {
        this.lettreCle = lettreCle;
    }

    public Float getQuantite() {
        return quantite;
    }

    public void setQuantite(Float quantite) {
        this.quantite = quantite;
    }

    public String getIdMedecinResponsable() {
        return idMedecinResponsable;
    }

    public void setIdMedecinResponsable(String idMedecinResponsable) {
        this.idMedecinResponsable = idMedecinResponsable;
    }

    public String getIdMedecinTraitant() {
        return idMedecinTraitant;
    }

    public void setIdMedecinTraitant(String idMedecinTraitant) {
        this.idMedecinTraitant = idMedecinTraitant;
    }

    public String getCodeMedecinResponsable() {
        return codeMedecinResponsable;
    }

    public void setCodeMedecinResponsable(String codeMedecinResponsable) {
        this.codeMedecinResponsable = codeMedecinResponsable;
    }

    public String getCodeMedecinTraitant() {
        return codeMedecinTraitant;
    }

    public void setCodeMedecinTraitant(String codeMedecinTraitant) {
        this.codeMedecinTraitant = codeMedecinTraitant;
    }

    public DataImportSigems() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDateActeHono() {
        return dateActeHono;
    }

    public void setDateActeHono(String dateActeHono) {
        this.dateActeHono = dateActeHono;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAnnee() {
        return annee;
    }

    public void setAnnee(String annee) {
        this.annee = annee;
    }

    public String getPratHonor() {
        return pratHonor;
    }

    public void setPratHonor(String pratHonor) {
        this.pratHonor = pratHonor;
    }

    public String getSpecialite() {
        return specialite;
    }

    public void setSpecialite(String specialite) {
        this.specialite = specialite;
    }

    public String getProdHonor() {
        return prodHonor;
    }

    public void setProdHonor(String prodHonor) {
        this.prodHonor = prodHonor;
    }

    public String getCoeffHonor() {
        return coeffHonor;
    }

    public void setCoeffHonor(String coeffHonor) {
        this.coeffHonor = coeffHonor;
    }

    public String getServ() {
        return serv;
    }

    public void setServ(String serv) {
        this.serv = serv;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DataImportSigems that = (DataImportSigems) o;

        if (id != that.id) return false;
        return code != null ? code.equals(that.code) : that.code == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (code != null ? code.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DataImportSigems{" +
                "id=" + id +
                ", dateActeHono='" + dateActeHono + '\'' +
                ", cinfode='" + code + '\'' +
                ", annee='" + annee + '\'' +
                ", pratHonor='" + pratHonor + '\'' +
                ", specialite='" + specialite + '\'' +
                ", prodHonor='" + prodHonor + '\'' +
                ", coeffHonor='" + coeffHonor + '\'' +
                ", serv='" + serv + '\'' +
                ", ippSigems=" + ippSigems +
                ", nomNaissance='" + nomNaissance + '\'' +
                ", nomUsuel='" + nomUsuel + '\'' +
                ", prenom='" + prenom + '\'' +
                ", numeroPA='" + numeroPA + '\'' +
                ", numero='" + numero + '\'' +
                ", idMedecinResponsable='" + idMedecinResponsable + '\'' +
                ", idMedecinTraitant='" + idMedecinTraitant + '\'' +
                ", codeMedecinResponsable='" + codeMedecinResponsable + '\'' +
                ", codeMedecinTraitant='" + codeMedecinTraitant + '\'' +
                ", lettreCle='" + lettreCle + '\'' +
                ", quantite=" + quantite +
                ", numDossier='" + numDossier + '\'' +

                ", codePratExec='" + codePratExec + '\'' +

                ", adeliPratExec='" + adeliPratExec + '\'' +
                '}';
    }

    public String getCodePratExec() {
        return codePratExec;
    }

    public void setCodePratExec(String codePratExec) {
        this.codePratExec = codePratExec;
    }

    public String getAdeliPratExec() {
        return adeliPratExec;
    }

    public void setAdeliPratExec(String adeliPratExec) {
        this.adeliPratExec = adeliPratExec;
    }
}
