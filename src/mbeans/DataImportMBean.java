package mbeans;
import com.google.gson.Gson;
import entities.DataImportSigems;
import sigemsreeduc.acte.SigeGateActeNgapSoapBindingStub;
import sigemsreeduc.acte.WsActeNgap;
import sigemsreeduc.acte.holders.WsActeNgapListeHolder;
import sigemsreeduc.dosbase.SigeGateDosBaseSoapBindingStub;
import sigemsreeduc.dosbase.WsDossierBase;
import sigemsreeduc.dosbase.holders.WsDossierBaseListeHolder;
import sigemsreeduc.sigegatepat.SigeGatePatSoapBindingStub;
import utilities.StubWebService;

import javax.ejb.EJB;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by mmvouma on 07/04/2017.
 */
@Named(value = "dataImportMBean")
@ViewScoped
public class DataImportMBean implements  Serializable{

    /**
     * information ejb
     */
    private InitialContext ctx = null;
    //private DataImportFromSigemsEjbBean dataImprtSigemEjb=null;
    private StubWebService stubWebservice;
    private final int NBRELIGNE=5;
    /**
     * information stub sigeGateDoseBase qui va interagir avec le webservice
     */
    private SigeGateDosBaseSoapBindingStub binding = null;

    /**
     * Stub SigeGateActeNgap
     */
    private SigeGateActeNgapSoapBindingStub bindingSigeGateActeNgap = null;

    /**
     * Stub SigeGatePat obtention du stub  pour toutes les méthodes
     */
    private static SigeGatePatSoapBindingStub bindingSigeGatePat =null;

    /**
     * Donnnée à importer dans la base de donnnée windev, mais avant, on affiche
     * les datas à l'utilisateur pour quelques traitement commme la consultation et autre....
     * Une fois la consultaion ok l'utilisateur pourra cliquer sur valider pour la validation de l'insertion
     * dans la base de donnée windev
     */
    private List<DataImportSigems> dataToImport;

    private List<WsDossierBase> listDossierSigems;

    /**
     * Liste des actes crées par les médécins
     */
    private List<WsActeNgap> listActeSigems;

    public List<WsDossierBase> getListDossierSigems() {
        return listDossierSigems;
    }

    public void setListDossierSigems(List<WsDossierBase> listDossierSigems) {
        this.listDossierSigems = listDossierSigems;
    }

    private static final String IDBASE="bddc_2stb";

    private Date dateDebut;
    private Date dateFin;
    private String simpleValue;

    /**
     * Liste types établissement
     */
    private List<SelectItem> listTypesEtablissements;
    private String typeEtabS;

    /**
     * Liste noms établissement
     */
    private List<SelectItem> listNomEtablissements;
    private String nomEtabS;

    private boolean disabledExport;

    public boolean isDisabledExport() {
        return disabledExport;
    }

    public void setDisabledExport(boolean disabledExport) {
        this.disabledExport = disabledExport;
    }

    public List<SelectItem> getListTypesEtablissements() {
        return listTypesEtablissements;
    }

    public void setListTypesEtablissements(List<SelectItem> listTypesEtablissements) {
        this.listTypesEtablissements = listTypesEtablissements;
    }

    public String getTypeEtabS() {
        return typeEtabS;
    }

    public void setTypeEtabS(String typeEtabS) {
        this.typeEtabS = typeEtabS;
    }

    public List<SelectItem> getListNomEtablissements() {
        return listNomEtablissements;
    }

    public void setListNomEtablissements(List<SelectItem> listNomEtablissements) {
        this.listNomEtablissements = listNomEtablissements;
    }

    public String getNomEtabS() {
        return nomEtabS;
    }

    public void setNomEtabS(String nomEtabS) {
        this.nomEtabS = nomEtabS;
    }

    public String getSimpleValue() {
        return simpleValue;
    }

    public void setSimpleValue(String simpleValue) {
        this.simpleValue = simpleValue;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    /**
     * Constructor Default
     */
    public DataImportMBean()
    {
        listDossierSigems = new ArrayList<>();
        listActeSigems = new ArrayList<>();
        dataToImport = new ArrayList<>();

        //setUpBeansEjb();
        stubWebservice = StubWebService.getInstance();
        binding = StubWebService.getBinding();
        bindingSigeGateActeNgap = StubWebService.getBindingStubSigeGateActeNgap();
        bindingSigeGatePat = StubWebService.getBindingStubSigeGatePat();

        //dataToImport = dataImprtSigemEjb.getDataToInsertIntoDbWindev();
        //System.err.println("web service gestion data " + dataImprtSigemEjb.getInformation());

        //researchDataToImport("10/02/2016", "10/03/2016");
        //**researchActeNgap("10/02/2016", "10/03/2016");
        //System.out.println("taille de la liste ");

        //System.out.println("donnée importer vers le webservice " + jsonData);
        //*System.out.println(StubWebService.formatData(formatDataToSend()));
        //*System.out.println(formatDataToSend());

        listTypesEtablissements = new ArrayList<>();
        listNomEtablissements = new ArrayList<>();
        initTypeEtab();
        setDisabledExport(true);

        //initNomEtablissement();
    }
    public List<DataImportSigems> getDataToImport() {
        return dataToImport;
    }
    public void setDataToImport(List<DataImportSigems> dataToImport) {
        this.dataToImport = dataToImport;
    }

    /**
     *Initialisation des beans de sessions ejb
     */
    /*public void setUpBeansEjb() {
        try {
            ctx = new InitialContext();
            Object ob = new InitialContext().lookup("java:global/IATReeducationMDear/IATReeducationMDejb/DataImportFromSigemsEjbEJB");
            if (ob != null) {
                dataImprtSigemEjb = (DataImportFromSigemsEjbBean) new InitialContext().lookup("java:global/IATReeducationMDear/IATReeducationMDejb/DataImportFromSigemsEjbEJB");
                System.out.println("non null " + dataImprtSigemEjb.getInformation());
            }
        } catch (NamingException e) {
            e.printStackTrace();

        }
    }*/

    /**
     * initialisation des types d'établissement
     */
    public void initTypeEtab()
    {
        listTypesEtablissements.add(new SelectItem("REED_CARD", "Activité Rééd. cardiaque"));
        listTypesEtablissements.add(new SelectItem("ACCUEIL_MED", "Activité Accueil Médical"));
    }

    /**
     * initialisation des noms des etablissements
     */
    public void initNomEtablissement(){
        listNomEtablissements.add(new SelectItem("SAINT_BASILE", "Saint Basile"));
        listNomEtablissements.add(new SelectItem("PLEIN CIEL", "PLEIN CIEL"));
        listNomEtablissements.add(new SelectItem("ESPERANCE", "ESPERANCE"));
    }
    public SigeGateDosBaseSoapBindingStub getBinding() {
        return binding;
    }

    /**
     *
     * @param dateDebut date de debut de la recherche
     * @param dateFin date de fin de la recherche
     */
    public void researchDataToImport(String dateDebut, String dateFin)
    {
        WsDossierBaseListeHolder resultatData=  new WsDossierBaseListeHolder();
        java.lang.String value = null;
        try {
            value = binding.searchDossier(IDBASE, 1, NBRELIGNE, new WsDossierBase(), dateDebut, dateFin, resultatData);
            System.out.println(" resultat " + value.length());
        } catch (RemoteException e) {
            // e.printStackTrace();
            System.out.println("ERREUR " + DataImportMBean.class.getName() + " Methode  researchDataToImport" + e.getMessage());
        }
        listDossierSigems = new ArrayList<>(Arrays.asList(resultatData.getValue().getListe()));
        for (WsDossierBase dosssier:resultatData.getValue().getListe()) {
            System.out.println(dosssier);
        }

    }

    public void researchActeNgap(String dateDebut, String dateFin)
    {
        WsActeNgapListeHolder listeResultatActe = new WsActeNgapListeHolder();
        WsActeNgap wsActe = new WsActeNgap();
        dataToImport = new ArrayList<>();
        String value ="";

        try {
            value=  bindingSigeGateActeNgap.searchActe(IDBASE, 1, NBRELIGNE, wsActe, dateDebut, dateFin, listeResultatActe);
            //bindingSigeGateActeNgap.searchActe(new String(), new Integer(0), new Integer(0), new WsActeNgap(), new String(), new String(), new WsActeNgapListeHolder());
            System.out.println("Value recherche acte " + value);
            if(listeResultatActe.getValue()!=null && listeResultatActe.getValue().getListe()!=null)
            {
                listActeSigems = new ArrayList<>(Arrays.asList(listeResultatActe.getValue().getListe()));
                for (WsActeNgap acte : listActeSigems)
                {
                    if(acte.getNumDossier()!=null)
                    {
                        System.out.println("acte definition " + acte);
                        DataImportSigems dataImportSigems = stubWebservice.informationPatAndFolderMD(acte.getNumDossier(), binding, bindingSigeGatePat);
                        dataImportSigems.setDateActeHono(acte.getExecuteDate());
                        dataImportSigems.setNumDossier(acte.getNumDossier());
                        try{
                            if(acte.getExecuteDate()!=null)
                            {
                                dataImportSigems.setCoeffHonor(String.valueOf(acte.getCoefficient()));
                            }
                        }catch(Exception e)
                        {
                            dataImportSigems.setCoeffHonor("");
                        }
                        dataImportSigems.setLettreCle(acte.getLettreCle());
                        dataImportSigems.setQuantite(acte.getQuantite());
                        dataToImport.add(dataImportSigems);
                    }

                }
            }


        } catch (RemoteException e) {
            //e.printStackTrace();
            System.out.println("ERREUR " + DataImportMBean.class.getName() + " Methode  researchActeNgap " + e.getMessage());
        }


    }
    public List<WsActeNgap> getListActeSigems() {
        return listActeSigems;
    }

    public void setListActeSigems(List<WsActeNgap> listActeSigems) {
        this.listActeSigems = listActeSigems;
    }

    public static String getIDBASE() {
        return IDBASE;
    }

    public void exporterData()
    {
        //System.out.println("date de  debut " + dateDebut + " date de fin " + dateFin + " simple date " + simpleValue);
        //*System.out.println(" simple date origine " + simpleValue + " date debut " + dateDebut + "origine dateFin " + dateFin);

        dataToImport = new ArrayList<>();
        SimpleDateFormat formatDateToString = new SimpleDateFormat("dd/MM/yyyy");
        String dteDebut ="", dteFin="";
        System.out.println("clique sur exporter ");
        if(dateDebut!=null && dateFin!=null)
        {
            dteDebut = formatDateToString.format(dateDebut);
            dteFin = formatDateToString.format(dateFin);

            if(dteDebut.length()>0 && dteFin.length()>0)
            {
                System.out.println("dateDebut " + dteDebut + " dateFin "+dteFin);
                dataToImport = stubWebservice.getDataActesEntreDeuxDates(dteDebut, dteFin);
                System.out.println("Taille de la liste " + dataToImport.size());
            }
        }

        if(dataToImport.size()>0)
        {
            setDisabledExport(false);
        }


    }
 //  " {'nbrePat':' "+	nNbrePatientAjouter + "', 'data': ' "+nNbreDataAjouter+ " ',  'error': ' "+ :getExceptionSurvenue() + " ' }"
    public void saveData()
    {
        SimpleDateFormat formatDateToString = new SimpleDateFormat("dd/MM/yyyy");
        String dteDebut ="", dteFin="";
        String resultatInsert ="";
        if(dateDebut!=null && dateFin!=null && this.getTypeEtabS()!=null && this.getTypeEtabS().length()>0 && this.getNomEtabS()!=null && this.getNomEtabS().length()>0)
        {
            dteDebut = formatDateToString.format(dateDebut);
            dteFin = formatDateToString.format(dateFin);
            System.out.println("date debut "+ dteDebut + " dteFin " + dteFin +  " taille "+ dataToImport.size());
            String nomEtab = this.getNomEtabS();
            String tyEtab = this.getTypeEtabS();
            String dataChaine =  StubWebService.formatData(formatDataToSend()).trim();

            resultatInsert = stubWebservice.executeSaveDataIntoWebService(dataChaine, dataToImport.size(), tyEtab, nomEtab, dteDebut, dteFin);
            System.out.println("resultat insertion "+ resultatInsert);

            dataToImport = new ArrayList<>();
        }
    }

    public String formatDataToSend()
    {
        Gson g = new Gson();
        return g.toJson(dataToImport);
    }

    public void actionMenuTypeEtablissement()
    {

        listNomEtablissements.clear();
        System.out.printf("clique sur le type etablissement " + this.getTypeEtabS());
        switch(this.getTypeEtabS())
        {
            case "REED_CARD":
                listNomEtablissements.add(new SelectItem("SAINT_BASILE", "SAINT_BASILE"));
                break;
            default:
                listNomEtablissements.add(new SelectItem("SAINT_BASILE", "Saint Basile"));
                listNomEtablissements.add(new SelectItem("PLEIN CIEL", "PLEIN CIEL"));
        }
    }

    /*public void doTheJob()
    {
        SimpleDateFormat formatDateToString = new SimpleDateFormat("dd/MM/yyyy");
        String dteDebut ="", dteFin="";
        if(dateDebut!=null && dateFin!=null)
        {
            dteDebut = formatDateToString.format(dateDebut);
            dteFin = formatDateToString.format(dateFin);

            if(dteDebut.length()>0 && dteFin.length()>0)
            {
                System.out.println("dateDebut " + dteDebut + " dateFin "+dteFin);
                dataToImport = stubWebservice.getDataActesEntreDeuxDates(dteDebut, dteFin);
                System.out.println("Taille de la liste " + dataToImport.size());
            }
        }

    }*/
}
