package mbeans;

import ejbs.DataImportEjbBean;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.Serializable;

/**
 * Created by mmvouma on 07/04/2017.
 */
@Named(value = "beansT")
@ViewScoped
public class BeansT implements Serializable{
    private String message;


    /*@EJB
    private DataImportEjbBean dataImportEjbBean;*/


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public BeansT() {

        message ="bienvenue";
        //System.out.println(" message bean " + dataImportEjbBean.testEjb());
    }

    public void testForm()
    {
        System.out.println("bienenvue " + message);
    }

    /*public void setUpBeansEjb() {
        try {
            ctx = new InitialContext();
            Object ob = new InitialContext().lookup("java:global/IATReeducationMDear/IATReeducationMDejb/DataImportFromSigemsEjbEJB");
            if (ob != null) {
                dataImportEjbBean = (DataImportEjbBean) new InitialContext().lookup("java:global/IATReeducationMDear/IATReeducationMDejb/DataImportFromSigemsEjbEJB");
                System.out.println("non null " + dataImportEjbBean.testEjb());
            }
        } catch (NamingException e) {
            e.printStackTrace();

        }
    }*/
}
