package mbeans;

import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

import java.io.Serializable;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 * Created by mmvouma on 07/04/2017.
 */

@Named(value = "menuMbeanT")
@ViewScoped
public class MenuMbeanT implements Serializable{

    /**
     * création des menus
     */
    private MenuModel model;
    private DefaultSubMenu importType;
    private DefaultMenuItem item;
    private DefaultSubMenu historiquePaiement;

    public MenuModel getModel() {
        return model;

    }

    public void setModel(MenuModel model) {
        this.model = model;
    }

    public MenuMbeanT() {
        model = new DefaultMenuModel();
        importType = new DefaultSubMenu("Import");
        DefaultMenuItem activReedCard = new DefaultMenuItem("Rééd cardiaque");
        activReedCard.setCommand("#menuMbean.getLinkReedCard}");
        activReedCard.setAjax(false);

        DefaultMenuItem activMed = new DefaultMenuItem("Accueil Médical");
        activMed.setAjax(false);
        activMed.setCommand("#{menuMbean.getLinkAccueilMed}");
        importType.addElement(activReedCard);
        importType.addElement(activMed);
        model.addElement(importType);

        historiquePaiement = new DefaultSubMenu("Historique");
        DefaultMenuItem itemPaiement = new DefaultMenuItem("Paiement");
        itemPaiement.setCommand("#{menuMbean.getLinkHistoriquePaiement}");
        itemPaiement.setAjax(false);
        historiquePaiement.addElement(itemPaiement);
        model.addElement(historiquePaiement);
    }
}
